import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { InicioPage } from '../pages/inicio/inicio';

declare var TestFairy: any;

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage:any = InicioPage;
  
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      TestFairy.begin("9b7eb62e6c844c945590c8ab5aa8416ba1927785");
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      //splashScreen.hide();

      setTimeout(() => {
        splashScreen.hide();
      }, 100);
    });
  }

}
