import { IonicImageViewerModule } from 'ionic-img-viewer';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

import { DoacoesMissoesPage } from '../pages/doacoes-missoes/doacoes-missoes';
import { DadosPessoaisPage } from '../pages/dados-pessoais/dados-pessoais';
import { MinhaContaPage } from '../pages/minha-conta/minha-conta';
import { ParabensPage } from '../pages/parabens/parabens';
import { InicioPage } from '../pages/inicio/inicio';
import { CadastrarCartaoPage } from '../pages/cadastrar-cartao/cadastrar-cartao';
import { MissoesPage } from '../pages/missoes/missoes';
import { DoePage } from '../pages/doe/doe';
import { LoginPage } from '../pages/login/login';
import { EventosPage } from '../pages/eventos/eventos';
import { CadastrarPage } from '../pages/cadastrar/cadastrar';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TextMaskModule } from 'angular2-text-mask';
import { MeusCartoesPage } from '../pages/meus-cartoes/meus-cartoes';
import { PrivacidadePage } from '../pages/privacidade/privacidade';
import { EsqueciSenhaPage } from '../pages/esqueci-senha/esqueci-senha';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { TransacaoProvider } from '../providers/transacao/transacao';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { AmbientesProvider } from '../providers/ambientes/ambientes';
import { IonCurrencyMaskModule } from '@pluritech/ion-currencymask';
import { CristolandiaPage } from '../pages/cristolandia/cristolandia';
import { ZonasulPage } from '../pages/zonasul/zonasul';
import { GentegrandePage } from '../pages/gentegrande/gentegrande';
import { IgrejasdosulPage } from '../pages/igrejasdosul/igrejasdosul';
import { TrocaPage } from '../pages/troca/troca';
import { UsoPage } from '../pages/uso/uso';
import { PoliticasPage } from '../pages/politicas/politicas';

@NgModule({
  declarations: [
    MyApp,
    PoliticasPage,
    TrocaPage,
    UsoPage,
    CristolandiaPage,
    ZonasulPage,
    GentegrandePage,
    IgrejasdosulPage,
    EsqueciSenhaPage,
    PrivacidadePage,
    MeusCartoesPage,
    DoacoesMissoesPage,
    DadosPessoaisPage,
    MinhaContaPage,
    ParabensPage,
    InicioPage,
    CadastrarCartaoPage,
    MissoesPage,
    DoePage,
    CadastrarPage,
    LoginPage,
    EventosPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    BrMaskerModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    TextMaskModule,
    IonCurrencyMaskModule,
    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PoliticasPage,
    TrocaPage,
    UsoPage,
    CristolandiaPage,
    ZonasulPage,
    GentegrandePage,
    IgrejasdosulPage,
    EsqueciSenhaPage,
    PrivacidadePage,
    MeusCartoesPage,
    DoacoesMissoesPage,
    DadosPessoaisPage,
    MinhaContaPage,
    ParabensPage,
    InicioPage,
    CadastrarCartaoPage,
    MissoesPage,
    DoePage,
    CadastrarPage,
    LoginPage,
    EventosPage,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    TransacaoProvider,
    AmbientesProvider
  ]
})
export class AppModule {}
