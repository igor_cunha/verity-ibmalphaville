import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrarCartaoPage } from './cadastrar-cartao';

@NgModule({
  imports: [
    IonicPageModule.forChild(CadastrarCartaoPage),
  ],
})
export class CadastrarCartaoPageModule {}
