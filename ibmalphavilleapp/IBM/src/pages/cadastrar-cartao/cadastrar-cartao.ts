import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';
import { AlertController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import * as $ from "jquery";
import { AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
/**
 * Generated class for the CadastrarCartaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastrar-cartao',
  templateUrl: 'cadastrar-cartao.html',
})
export class CadastrarCartaoPage {

  masks: any;
  cardNumber: any = "";
  titular:string = "";
  validade: string = ""; 
  codigo:string = "";
  public teste : any = false;
  loader;
  constructor(public loadingController: LoadingController, private ambiente : AmbientesProvider, private auth : AuthServiceProvider, public http : HttpClient, public alerCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {

    this.masks = {
      cardNumber: [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
      cardExpiry: [/[0-1]/, /\d/, '/', /[1-2]/, /\d/],
      orderCode: [/\d/, /\d/, /\d/, /\d/]
  };
    
  }


  ionViewDidLoad() {
    
  }

  public showCode = false;

  blurCartaoInvalido() {
    if(this.cardNumber.startsWith(3) || this.cardNumber.startsWith(4) || this.cardNumber.startsWith(5) || this.cardNumber.startsWith(6) ) {
      // let error = document.querySelectorAll(".formCadastro");
      // Object.keys(error).map((key) => {
      //   error[key].style.border = 'transparent';
      // });
      // let elements = document.querySelectorAll(".numeroCartaoInvalido");
      // Object.keys(elements).map((key) => {
      //   elements[key].style.display = 'none';
      // });
    } else {
      // let error = document.querySelectorAll(".formCadastro");
      // Object.keys(error).map((key) => {
      //   error[key].style.border = '1px solid #ea6153';
      // });
      // let elements = document.querySelectorAll(".numeroCartaoInvalido");
      // Object.keys(elements).map((key) => {
      //   elements[key].style.display = 'block';
      // });
    }
  }

  hideInput() {

      if(this.cardNumber !== "" && this.cardNumber.startsWith(3) || this.cardNumber.startsWith(4) || this.cardNumber.startsWith(5) || this.cardNumber.startsWith(6) 
      && this.validade !== "" && this.titular !== "") {
        
        this.teste = true;

        //let elements = document.querySelectorAll(".preCode");
        //Object.keys(elements).map((key) => {
        //  elements[key].style.display = 'none';
        //});
    
        //let frontCards = document.querySelectorAll(".frontCard");
        //Object.keys(frontCards).map((key) => {
        //  frontCards[key].style.opacity = '0';
        //});
            this.showCode = true;
      } else {
        // let error = document.querySelectorAll(".formCadastro");
        // Object.keys(error).map((key) => {
        //   error[key].style.border = '1px solid #ea6153';
        // });
        // let elements = document.querySelectorAll(".numeroCartaoInvalido");
        // Object.keys(elements).map((key) => {
        //   elements[key].style.display = 'block';
        // });
      } 
  }

  frontAgain() {
    this.showCode = false;
    this.teste = false;
    //let elements = document.querySelectorAll(".preCode");
    //Object.keys(elements).map((key) => {
    //  elements[key].style.display = 'block';
    //});
    //let frontCards = document.querySelectorAll(".frontCard");
    //Object.keys(frontCards).map((key) => {
    //  frontCards[key].style.opacity = '2';
    //});
  }
  save(senha:string){
    this.showLoading();
    this.codigo = this.codigo.replace(/[^a-zA-Z0-9 -]/g, '');
    this.cardNumber = this.cardNumber.replace("__", "");
    //formata validade para enviar para o servidor
    let val = "20"+this.validade.substring(3)+"-"+this.validade.substring(0,2);

    let user = this.auth.getUserInfo();
    user.password = senha;

    let cartao = {
      numeroCartao : this.cardNumber,
      vencimento : val,
      codSeguranca : this.codigo,
      titular : this.titular,
      usuario : user
    }
    return new Promise(resolve => {
    this.http.post(this.ambiente.urlBase+"cartao", cartao).subscribe(data => {
      resolve(data);
      this.dismissLoading();
      let alert = this.alerCtrl.create({
        title: 'Sucesso!',
        subTitle: 'Seu cartão foi cadastrado com sucesso!',
        buttons: [{
          text: 'Ok',
          handler: data => {
            this.navCtrl.popToRoot();
          }
        }]
      });
      alert.present();
    }, err => {
      if(err.status == '401'){
        this.dismissLoading();
        this.auth.logOut();
        this.navCtrl.popToRoot();
      }else{
        this.dismissLoading();
        let alert = this.alerCtrl.create({
          title: err.error,
          subTitle: 'Certifique-se que você digitou as informações corretas.',
          buttons: ['Ok']
        });
        alert.present();
        this.frontAgain();
      }
       if (err.status === 0){
        this.dismissLoading();
        let alert = this.alerCtrl.create({
          title: 'Ooops...!',
          subTitle: 'Algo de errado aconteceu!',
          buttons: ['Ok']
        });
        alert.present();
        this.frontAgain();
      } else if (err.status === 500){
        this.dismissLoading();
        let alert = this.alerCtrl.create({
          title: 'Ooops...!',
          subTitle: 'Algo de errado aconteceu!',
          buttons: ['Ok']
        });
        alert.present();
        this.frontAgain();
      }
      
    });
  });
    
  }
  doAlert() {
    if(this.codigo !== "") {
      // let frontCards = document.querySelectorAll(".formCodigo");
      // Object.keys(frontCards).map((key) => {
      //   frontCards[key].style.border = 'transparent';
      // });
      // let dataValidade = document.querySelectorAll(".codigoSeguranca");
      // Object.keys(dataValidade).map((key) => {
      //   dataValidade[key].style.display = 'none';
      // });
      let alert = this.alerCtrl.create({
        title: 'Digite sua senha da IBMAlphaville.',
        message: 'Por favor entre com sua senha cadastrada no aplicativo para confirmar a operação.',
        inputs:[{
          name: 'senha',
          placeholder: 'Digite sua senha',
          type: 'password'
        }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel'
          },
          {
            text: 'Confirmar',
            handler: data => {
              this.save(data.senha);
            }
          }
        ]
        
      });
      alert.present()
      
    } else {
      // let frontCards = document.querySelectorAll(".formCodigo");
      // Object.keys(frontCards).map((key) => {
      //   frontCards[key].style.border = '1px solid #ea6153';
      // });
      // let dataValidade = document.querySelectorAll(".codigoSeguranca");
      // Object.keys(dataValidade).map((key) => {
      //   dataValidade[key].style.display = 'block';
      // });
    }
    
  }

  parabens() {
    this.navCtrl.popToRoot();
  }

  showLoading() {
    if(!this.loader){
        this.loader = this.loadingController.create({
            content: 'carregando...'
        });
        this.loader.present();
    }
  }
  dismissLoading(){
    if(this.loader){
        this.loader.dismiss();
        this.loader = null;
    }
  }
}



