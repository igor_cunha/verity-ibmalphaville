import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrarPage } from './cadastrar';

@NgModule({
  imports: [
    IonicPageModule.forChild(CadastrarPage),
  ],
})
export class CadastrarPageModule {}
