import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider, User } from '../../providers/auth-service/auth-service';
import * as $ from "jquery";
import { ParabensPage } from '../parabens/parabens';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
import { UsoPage } from '../uso/uso';
import { PrivacidadePage } from '../privacidade/privacidade';
import { TrocaPage } from '../troca/troca';
/**
 *
 * Generated class for the CadastrarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastrar',
  templateUrl: 'cadastrar.html',
})
export class CadastrarPage {
  private API_URL = this.ambiente.urlBase+'usuarios';

  private name:string = "";
  public email:string = "";
  public password:string = "";
  public cpf:string = "";
  public phone:string = "";
  
  public validacaoEmail : any = false;
  public validacaoEmail2 : any = false;
  public validacaoPassword : any = false;
  public validacaoCPF : any = false;
  public validacaoPhone : any = false;
  
  constructor(private ambiente : AmbientesProvider, public alerCtrl: AlertController, private storage: Storage, private auth : AuthServiceProvider, public http : HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    
   }

   public type = 'password';
   public showPass = false;

   showPassword() {
     this.showPass = !this.showPass;
  
     if(this.showPass){
       this.type = 'text';
     } else {
       this.type = 'password';
     }
   }
  createAccount(name: string, email: string, password: string, cpf: string, phone: string) {
    //this.navCtrl.push(CadastrarCartaoPage ,{name: name, email: email, password: password, cpf: cpf, phone: phone});
    // let error = document.querySelectorAll(".emailAlreadyExist");
    // Object.keys(error).map((key) => {
    //   error[key].style.display = 'none';
    // });
    // let emailInvalid = document.querySelectorAll(".emailInvalid");
    // Object.keys(emailInvalid).map((key) => {
    //   emailInvalid[key].style.display = 'none';
    // });
    // let cpfInvalid = document.querySelectorAll(".cpfInvalid");
    // Object.keys(cpfInvalid).map((key) => {
    //   cpfInvalid[key].style.display = 'none';
    // });
    // let senhaInvalid = document.querySelectorAll(".senhaInvalida");
    // Object.keys(senhaInvalid).map((key) => {
    //   senhaInvalid[key].style.display = 'none';
    // });
    if(name !== "" && email !== "" && password !== "" && cpf !== "" && phone !== "" && this.senhaForte(password)) {
      return new Promise((resolve, reject) => {
                var usuario = {
                  name: name,
                  email: email,
                  password: password,
                  cpf: cpf,
                  phone: phone
                };
           
                this.http.post(this.API_URL, usuario)
                  .subscribe((result: any) => {
                    let u = new User(result);
                    this.auth.register(u);
                    this.storage.set('name', result.name);
                    this.storage.set('cpf', result.cpf);
                    this.storage.set('phone', result.phone);
                    this.storage.set('email', result.email);
                    this.navCtrl.push(ParabensPage);
                  },
                  (error) => {
                    // let err = document.querySelectorAll(".cpfInvalid");
                    // Object.keys(err).map((key) => {
                    //   err[key].style.display = 'none';
                    // });
                    // let wrong = document.querySelectorAll(".emailInvalid");
                    // Object.keys(wrong).map((key) => {
                    //   wrong[key].style.display = 'none';
                    // });
                    if(error.error === "O cpf não é válido.") {
                      this.validacaoCPF = true;
                      // let frontCards = document.querySelectorAll(".inputCPF");
                      // Object.keys(frontCards).map((key) => {
                      //   frontCards[key].style.border = '1px solid #ea6153';
                      // });
                      // let error = document.querySelectorAll(".cpfInvalid");
                      // Object.keys(error).map((key) => {
                      //   error[key].style.display = 'block';
                      // });
                    }else{
                      this.validacaoCPF = false;
                    } 
                    if(error.error === "O email não é válido") {
                      this.validacaoEmail = true;
                      // let frontCards = document.querySelectorAll(".inputEmailu");
                      // Object.keys(frontCards).map((key) => {
                      //   frontCards[key].style.border = '1px solid #ea6153';
                      // });
                      // let error = document.querySelectorAll(".emailInvalid");
                      // Object.keys(error).map((key) => {
                      //   error[key].style.display = 'block';
                      // });
                    }else{
                      this.validacaoEmail = false;
                    }
                    if(error.error === "E-mail já cadastrado em nossa base de dados.") {
                      this.validacaoEmail2 = true;
                      // let frontCards = document.querySelectorAll(".inputEmailu");
                      // Object.keys(frontCards).map((key) => {
                      //   frontCards[key].style.border = '1px solid #ea6153';
                      // });
                      // let error = document.querySelectorAll(".emailAlreadyExist");
                      // Object.keys(error).map((key) => {
                      //   error[key].style.display = 'block';
                      // });
                    }else{
                      this.validacaoEmail2 = false;
                    }
                    if(error.error === "O telefone não é válido") {
                      this.validacaoPhone = true;
                      // let frontCards = document.querySelectorAll(".inputEmailu");
                      // Object.keys(frontCards).map((key) => {
                      //   frontCards[key].style.border = '1px solid #ea6153';
                      // });
                      // let error = document.querySelectorAll(".emailAlreadyExist");
                      // Object.keys(error).map((key) => {
                      //   error[key].style.display = 'block';
                      // });
                    }else{
                      this.validacaoPhone = false;
                    }
                    

                    if(error.error === "Verifique suas informações.") {
                      let alert = this.alerCtrl.create({
                        title: 'Ooops...',
                        message: 'CPF ou Telefone já cadastrados ou inválidos',
                        buttons: ['Ok']
                      });
                      alert.present()
                    }
                    if (error.status === 500) {
                      let alert = this.alerCtrl.create({
                        title: 'Ooops...',
                        message: 'Algo de errado ocorreu!',
                        buttons: ['Ok']
                      });
                      alert.present()
                    } 
                  });
              });
            } 
           
    
  }

  public senhaForte(senha:string){
    let isForte = senha.match("([0-9].*[a-zA-Z])|([a-zA-Z].*[0-9])") && senha.length > 5;
    if(isForte== false || isForte==null){
      this.validacaoPassword = true;
      // let frontCards = document.querySelectorAll(".inputSenha");
      // Object.keys(frontCards).map((key) => {
      //   frontCards[key].style.border = '1px solid #ea6153';
      // });
      // let error = document.querySelectorAll(".senhaInvalida");
      // Object.keys(error).map((key) => {
      //   error[key].style.display = 'block';
      // });
    }else{
      this.validacaoPassword = false;
    }
    return isForte;
  }
  ionViewDidLoad() {
  }

  public fContructMessageToPost(ServiceToBeCalled , ContentJSON){
    ContentJSON = JSON.stringify(ContentJSON);
    var resp = {};
    resp['UserHash'] = ''
    resp['softwareversion'] = '2.2.0'
    resp['softwarename'] = 'IsyBuy'
    resp['ServiceToBeCalled'] = ServiceToBeCalled
    resp['ContentJSON'] = ContentJSON
   
    return resp
   }

   uso() {
    this.navCtrl.push(UsoPage);
   }

   privacidade() {
    this.navCtrl.push(PrivacidadePage);
   }

   troca() {
     this.navCtrl.push(TrocaPage);
   }
    
}



