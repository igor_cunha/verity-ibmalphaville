import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CristolandiaPage } from './cristolandia';

@NgModule({
  imports: [
    IonicPageModule.forChild(CristolandiaPage),
  ],
})
export class CristolandiaPageModule {}
