import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DadosPessoaisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dados-pessoais',
  templateUrl: 'dados-pessoais.html',
})
export class DadosPessoaisPage {
  public name : any = "";
  public cpf : any = "";
  public phone : any = "";
  public email : any = "";

  constructor(private storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.storageInformation();
  }

  storageInformation() : any {
    this.storage.get('name').then((val) => {
      this.name = val;
      return val;
    });
    this.storage.get('cpf').then((val) => {
      this.cpf = val;
      return val;
    });
    this.storage.get('phone').then((val) => {
      this.phone = val;
      return val;
    });
    this.storage.get('email').then((val) => {
      this.email = val;
      return val;
    });
  }

}
