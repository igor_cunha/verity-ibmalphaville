import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoacoesMissoesPage } from './doacoes-missoes';

@NgModule({
  imports: [
    IonicPageModule.forChild(DoacoesMissoesPage),
  ],
})
export class DoacoesMissoesPageModule {}
