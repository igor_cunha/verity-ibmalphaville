import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import { HttpClient} from '@angular/common/http';
import { AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
/**
 * Generated class for the DoacoesMissoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-doacoes-missoes',
  templateUrl: 'doacoes-missoes.html',
})
export class DoacoesMissoesPage {
  logged: any;
  receipts:any;

  loader = this.loadingController.create({
      content: "carregando..."
    });
  constructor(private ambiente : AmbientesProvider, public loadingController: LoadingController,public alerCtrl: AlertController, public http: HttpClient, private auth : AuthServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.logged = this.auth.getUserInfo();
  }

  ionViewWillLoad() {
    this.findReceipt(this.logged.hash)
  }
  findReceipt (hash:string) :any{
    this.loader.present();
    let cards;
    return new Promise(resolve => {
        this.http.get(this.ambiente.urlBase+"transacao/"+hash).subscribe(data => {
          this.receipts = data;
          resolve(data);
          this.loader.dismiss();
        }, err => {
          this.loader.dismiss();
          if(err.status == '401'){
            this.auth.logOut();
            this.navCtrl.popToRoot();
          }
          else if(err.status != '404'){
            this.alerCtrl.create({
              title: 'Ooops...',
              message: 'Algo de errado aconteceu. Tente novamente mais tarde.',
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    
                  }
                }
              ]
              
            }).present();
          }
        });
      });
  }
}
