import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoePage } from './doe';

@NgModule({
  imports: [
    IonicPageModule.forChild(DoePage),
  ],
})
export class DoePageModule {}
