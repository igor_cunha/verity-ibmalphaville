import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EsqueciSenhaPage } from './esqueci-senha';

@NgModule({
  imports: [
    IonicPageModule.forChild(EsqueciSenhaPage),
  ],
})
export class EsqueciSenhaPageModule {}
