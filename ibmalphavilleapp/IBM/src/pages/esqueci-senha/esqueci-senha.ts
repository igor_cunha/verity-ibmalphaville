import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider, User } from '../../providers/auth-service/auth-service';
import * as $ from "jquery";
import { SyncAsync } from '@angular/compiler/src/util';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
/**
 * Generated class for the EsqueciSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esqueci-senha',
  templateUrl: 'esqueci-senha.html',
})
export class EsqueciSenhaPage {

  public email : any = "";
  constructor(private ambiente : AmbientesProvider, private auth : AuthServiceProvider, public alerCtrl: AlertController, public http : HttpClient, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  private API_URL = this.ambiente.urlBase+'login/esqueciSenha';

  enviarSenha(email: string) {
    if(email === "" || email === undefined) {
      let alert = this.alerCtrl.create({
        title: 'E-mail inválido',
        message: 'Informe um e-mail já cadastrado!',
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              
            }
          }
        ]
        
      });
      alert.present()
    } else {
      return new Promise((resolve, reject) => {
        
        var usuario = {
          email: email
        };
   
        this.http.post(this.API_URL, usuario)
          .subscribe((result: any) => {
            let alert = this.alerCtrl.create({
              title: 'Pronto!',
              message: 'Foi enviado um link para alteração de senha em seu e-mail',
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    this.navCtrl.popToRoot();
                  }
                }
              ]
            });
            alert.present()
          },
          (error) => {

            if(error.status === 400) {
              let alert = this.alerCtrl.create({
                title: 'E-mail inválido',
                message: 'Informe um e-mail já cadastrado!',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      
                    }
                  }
                ]
                
              });
              alert.present()
            } else {
              let alert = this.alerCtrl.create({
                title: 'Ooops...',
                message: 'Algo de errado aconteceu. Tente novamente mais tarde!',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      
                    }
                  }
                ]
                
              });
              alert.present()
            }
            



          });
      });
    }
  }

}
