import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';
import { HttpClient} from '@angular/common/http';
import { TransacaoProvider } from '../../providers/transacao/transacao';
import { AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html',
})
export class EventosPage {

  cards: any;
  logged: any;

  public quantidade : any = 1;
  public codCard : any;
  public value : any = 0;
  public eventObj : any;
  public eventPart: boolean = false;
  public varFinal : any;

  loader;
  constructor(public toastCtrl: ToastController, private ambiente : AmbientesProvider, public loadingController: LoadingController, private auth : AuthServiceProvider,private trasacao : TransacaoProvider, public alerCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: HttpClient) {
    this.logged = this.auth.getUserInfo();
  }

  ionViewWillLoad() {
    this.findCards(this.logged.hash);
    this.eventObj = this.auth.getEventInfo();
    this.value = this.eventObj.valor;
    this.varFinal = this.value;
  }

  plusTicket() {
    let qtdRestante = this.eventObj.qtdIngresso - this.eventObj.qtdIngressoVendido;
    if(this.quantidade < qtdRestante) {
      this.quantidade = this.quantidade + 1;  
      this.varFinal = this.value * this.quantidade;
    }
    
  }

  lessTicket() {
    if(this.quantidade >= 2) {
      this.quantidade = this.quantidade - 1;
      this.varFinal = this.value * this.quantidade;
    }
  }

  buyEvent() {
    if(this.cards) {
      this.eventPart = true;
      let error = document.querySelectorAll(".button-inner");
      Object.keys(error).map((key) => {
        error[key].style.display = 'none';
      });
    } else {
      let prompt = this.alerCtrl.create({
        title: 'Cadastrar um cartão?',
        message: "Você não pode doar sem ter um cartão cadastrado. Deseja cadastrar um agora?",
        buttons: [
          {
            text: 'Não',
            handler: data => {
            }
          },
          {
            text: 'Cadastrar',
            handler: data => {
              this.navCtrl.push(CadastrarCartaoPage);
            }
          }
        ]
      });
      prompt.present();
    }
  }

  backEventDescription() {
    this.eventPart = false;
    let error = document.querySelectorAll(".button-inner");
    Object.keys(error).map((key) => {
      error[key].style.display = 'block';
    });
  }

  blurValor() {
    if(this.value !== "" && this.value !== undefined) {
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = 'transparent';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'none';
      });
    } else {
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = '1px solid #ea6153';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'block';
      });
    }
  }

  findCards (hash:string) :any{
    this.showLoading();
    return new Promise(resolve => {
        this.http.get(this.ambiente.urlBase+"cartao/me/"+hash).subscribe(data => {
          this.cards = data;
          resolve(data);
          this.dismissLoading();
        }, err => {
          this.dismissLoading();
          if(err.status == '401'){
            this.auth.logOut();
            this.navCtrl.popToRoot();
          }
        });
      });
  }
  doConfirm(value:any,codCard:number, qtdTicket: any) {
    let idProjeto:number = this.eventObj.id;//id correspondente ao id do projeto no banco
    if(value !== "" && value !== undefined) {
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = 'transparent';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'none';
      });
      if(codCard !== undefined && codCard !== 0) {
        let frontCards = document.querySelectorAll(".cardInput");
        Object.keys(frontCards).map((key) => {
          frontCards[key].style.border = 'transparent';
        });
        let error = document.querySelectorAll(".comboCartao");
        Object.keys(error).map((key) => {
          error[key].style.display = 'none';
        });
        let confirm = this.alerCtrl.create({
          title: 'Deseja confirmar a compra dos ingressos?',
          message: 'Ao confirmar, será enviado ao seu e-mail os ingressos do evento '+this.eventObj.titulo+' e o comprovante da compra.',
          inputs:[{
            name: 'senha',
            placeholder: 'Digite sua senha',
            type: 'password'
          }
          ],
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {
              }
            },
            {
              text: 'Confirmar',
              handler: data => {
                let user = this.auth.getUserInfo();
                user.password = data.senha;

                this.trasacao.transfer(user, value, codCard, idProjeto, qtdTicket)//TODO: passar idProjeto e qtdIngresso
                  .then(data => {
                    let ok = this.alerCtrl.create({
                    title: 'Sucesso!',
                    message: 'Pagamento realizado com sucesso! Em breve, você receberá um e-mail com seu(s) ingresso(s).',
                    buttons: [{
                      text: 'Sair',
                      handler: () => {
                        clearTimeout(timeLogout);
                        this.auth.logOut();
                        this.navCtrl.popToRoot();
                      }
                    },{
                      text: 'Continuar',
                      handler: () => {
                        clearTimeout(timeLogout);
                        this.navCtrl.popToRoot();
                      }
                    }]
                  });
                  ok.present();
                  let global = this;
                  let timeLogout = setTimeout(function(){ 
                      ok.dismiss();
                      global.auth.logOut();
                      global.navCtrl.popToRoot(); 
                    },6000);
                  }).catch(a => {
                    this.auth.logOut();
                    this.navCtrl.popToRoot();});
              }
            }
          ]
        });
        confirm.present()
      } else {
      //CARD
      let frontCards = document.querySelectorAll(".cardInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = '1px solid #ea6153';
      });
      let error = document.querySelectorAll(".comboCartao");
      Object.keys(error).map((key) => {
        error[key].style.display = 'block';
      });
      }
    } else {
      //MONEY
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = '1px solid #ea6153';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'block';
      });
    }

    
  }

  cadastrarCartao() {
    this.navCtrl.push(CadastrarCartaoPage);
  }
  showLoading() {
    if(!this.loader){
        this.loader = this.loadingController.create({
            content: 'carregando...'
        });
        this.loader.present();
    }
  }
  dismissLoading(){
    if(this.loader){
        this.loader.dismiss();
        this.loader = null;
    }
  }

  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Utilizamos a plataforma de pagamento IsyBuy',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

}
