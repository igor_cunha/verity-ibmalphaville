import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GentegrandePage } from './gentegrande';

@NgModule({
  imports: [
    IonicPageModule.forChild(GentegrandePage),
  ],
})
export class GentegrandePageModule {}
