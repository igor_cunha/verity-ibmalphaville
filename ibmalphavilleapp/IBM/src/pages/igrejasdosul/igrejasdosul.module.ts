import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IgrejasdosulPage } from './igrejasdosul';

@NgModule({
  imports: [
    IonicPageModule.forChild(IgrejasdosulPage),
  ],
})
export class IgrejasdosulPageModule {}
