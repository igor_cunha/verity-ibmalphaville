import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';
import { InicioPage } from '../inicio/inicio';
import { LoginPage } from '../login/login';
import { HttpClient} from '@angular/common/http';
import { TransacaoProvider } from '../../providers/transacao/transacao';
import { AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { ToastController } from 'ionic-angular';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';

/**
 * Generated class for the IgrejasdosulPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-igrejasdosul',
  templateUrl: 'igrejasdosul.html',
})
export class IgrejasdosulPage {

  cards: any;
  logged: any;
  public codCard : any;
  public value : any = "";
  loader;
  constructor(private ambiente : AmbientesProvider, public toastCtrl: ToastController, public loadingController: LoadingController, private auth : AuthServiceProvider,public trasacao : TransacaoProvider, public alerCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public http: HttpClient) {
    this.logged = this.auth.getUserInfo();
  }

  ionViewWillLoad() {
    this.findCards(this.logged.hash);
    
  }

  blurValor() {
    if(this.value !== "" && this.value !== undefined) {
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = 'transparent';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'none';
      });
    } else {
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = '1px solid #ea6153';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'block';
      });
    }
  }

  findCards (hash:string) :any{
    this.showLoading();
    return new Promise(resolve => {
        this.http.get(this.ambiente.urlBase+"cartao/me/"+hash).subscribe(data => {
          this.cards = data;
          resolve(data);
          this.dismissLoading();
        }, err => {
          this.dismissLoading();
          if(err.status == '401'){
            this.auth.logOut();
            this.navCtrl.popToRoot();
          }
          if(err.status == '404'){
            let prompt = this.alerCtrl.create({
              title: 'Cadastrar um cartão?',
              message: "Você não pode doar sem ter um cartão cadastrado. Deseja cadastrar um agora?",
              buttons: [
                {
                  text: 'Não',
                  handler: data => {
                    this.navCtrl.popToRoot();
                  }
                },
                {
                  text: 'Cadastrar',
                  handler: data => {
                    this.navCtrl.push(CadastrarCartaoPage);
                  }
                }
              ]
            });
            prompt.present();
          }
        });
        
      });
  }
  doConfirm(value:string,codCard:number) {
    if(this.cards) {
    const idProjeto:number = 10005;//id correspondente ao id do projeto no banco

    if(value !== "" && value !== undefined) {
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = 'transparent';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'none';
      });
      if(codCard !== undefined && codCard !== 0) {
        let frontCards = document.querySelectorAll(".cardInput");
        Object.keys(frontCards).map((key) => {
          frontCards[key].style.border = 'transparent';
        });
        let error = document.querySelectorAll(".comboCartao");
        Object.keys(error).map((key) => {
          error[key].style.display = 'none';
        });

        let confirm = this.alerCtrl.create({
          title: 'Digite sua senha do IBMAlphaville para confirmar.',
          message: 'Ao confirmar, nossa plataforma de pagamento IsyBuy enviará ao seu e-mail o comprovante da contribuição.',
          inputs:[{
            name: 'senha',
            placeholder: 'Digite sua senha',
            type: 'password'
          }
          ],
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {
              }
            },
            {
              text: 'Confirmar',
              handler: data => {
                let user = this.auth.getUserInfo();
                user.password = data.senha;
                this.trasacao.transfer(user, value, codCard, idProjeto)
                  .then(data => {
                    let ok = this.alerCtrl.create({
                    title: 'Sucesso!',
                    message: 'Pagamento realizado com sucesso! Em breve, você receberá um e-mail da nossa plataforma de pagamento IsyBuy com a confirmação '+
                    'da contribuição.',
                    buttons: [{
                      text: 'Sair',
                      handler: () => {
                        clearTimeout(timeLogout);
                        this.auth.logOut();
                        this.navCtrl.popToRoot();
                      }
                    },{
                      text: 'Continuar',
                      handler: () => {
                        clearTimeout(timeLogout);
                        this.navCtrl.popToRoot();
                      }
                    }]
                  });
                  ok.present();
                  let global = this;
                  let timeLogout = setTimeout(function(){ 
                      ok.dismiss();
                      global.auth.logOut();
                      global.navCtrl.popToRoot(); 
                    },6000);
                  }).catch(a => {
                    this.auth.logOut();
                    this.navCtrl.popToRoot();});
              }
            }
          ]
        });
        confirm.present()
      } else {
      //CARD
      let frontCards = document.querySelectorAll(".cardInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = '1px solid #ea6153';
      });
      let error = document.querySelectorAll(".comboCartao");
      Object.keys(error).map((key) => {
        error[key].style.display = 'block';
      });
      }
    } else {
      //MONEY
      let frontCards = document.querySelectorAll(".moneyInput");
      Object.keys(frontCards).map((key) => {
        frontCards[key].style.border = '1px solid #ea6153';
      });
      let error = document.querySelectorAll(".valor");
      Object.keys(error).map((key) => {
        error[key].style.display = 'block';
      });
    }

  } else {
      let prompt = this.alerCtrl.create({
        title: 'Cadastrar um cartão?',
        message: "Você não pode doar sem ter um cartão cadastrado. Deseja cadastrar um agora?",
        buttons: [
          {
            text: 'Não',
            handler: data => {
              this.navCtrl.popToRoot();
            }
          },
          {
            text: 'Cadastrar',
            handler: data => {
              this.navCtrl.push(CadastrarCartaoPage);
            }
          }
        ]
      });
      prompt.present();
    
  }
  }

  cadastrarCartao() {
    this.navCtrl.push(CadastrarCartaoPage);
  }
  showLoading() {
    if(!this.loader){
        this.loader = this.loadingController.create({
            content: 'carregando...'
        });
        this.loader.present();
    }
  }
  dismissLoading(){
    if(this.loader){
        this.loader.dismiss();
        this.loader = null;
    }
  }

  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Utilizamos a plataforma de pagamento IsyBuy',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

}
