import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DoePage } from '../doe/doe';
import { MissoesPage } from '../missoes/missoes';
import { LoginPage } from '../login/login';
import { MinhaContaPage } from '../minha-conta/minha-conta';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HttpClient } from '@angular/common/http';
import { EventosPage } from '../eventos/eventos';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
import { CristolandiaPage } from '../cristolandia/cristolandia';
import { ZonasulPage } from '../zonasul/zonasul';
import { GentegrandePage } from '../gentegrande/gentegrande';
import { IgrejasdosulPage } from '../igrejasdosul/igrejasdosul';
/**
 * Generated class for the InicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})

export class InicioPage {
  constructor(private ambiente : AmbientesProvider, private auth : AuthServiceProvider, public http : HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    
   }
  
  numEventos: number = 0;
  hash:string;
  name: string;
  public list : any;
  public timeLogout : any;
  public teste : any = false;

  callEventos() {
      return new Promise(resolve => {
          this.http.get(this.ambiente.urlBase+"projeto").subscribe(data => {
          this.list = data;
          }, err => {
            if(err.status == '401'){
              this.auth.logOut();
              this.navCtrl.popToRoot();
            }
          });
        });
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {

    this.callEventos();
    
    if(this.auth.getUserInfo() == undefined) {
      this.teste = false;
      // let frontCards = document.querySelectorAll(".userIcon");
      // Object.keys(frontCards).map((key) => {
      //   frontCards[key].style.display = 'block';
      // });
      // let userIconName = document.querySelectorAll(".userIconName");
      // Object.keys(userIconName).map((key) => {
      //   userIconName[key].style.display = 'none';
      // });
      // let frontCard = document.querySelectorAll(".nameUser");
      // Object.keys(frontCard).map((key) => {
      //   frontCard[key].style.display = 'none';
      // });
    }
    if(this.auth.getUserInfo() != undefined){
      this.teste = true;
      this.hash = this.auth.getUserInfo().hash;
      this.name = this.auth.getUserInfo().name;
      
      let global = this;
      this.timeLogout = setTimeout(function() {
        global.teste = false;
        global.auth.logOut();
      },11000);

      // let frontCards = document.querySelectorAll(".userIcon");
      // Object.keys(frontCards).map((key) => {
      //   frontCards[key].style.display = 'none';
      // });
      // let userIconName = document.querySelectorAll(".userIconName");
      // Object.keys(userIconName).map((key) => {
      //   userIconName[key].style.display = 'block';
      // });
      // let frontCard = document.querySelectorAll(".nameUser");
      // Object.keys(frontCard).map((key) => {
      //   frontCard[key].style.display = 'block';
      // });
    }
  }

  doe() {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      clearTimeout(this.timeLogout);
      this.navCtrl.push(LoginPage);
    }else{
      clearTimeout(this.timeLogout);
      this.navCtrl.push(DoePage);
    }
  }

  missoes() {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      clearTimeout(this.timeLogout);
      this.navCtrl.push(LoginPage);
    }else{
      clearTimeout(this.timeLogout);
      this.navCtrl.push(MissoesPage);
    }
  }

  login() {
    clearTimeout(this.timeLogout);
    this.navCtrl.push(LoginPage);
  }

  parabens() {
    clearTimeout(this.timeLogout);
    this.navCtrl.push(MinhaContaPage)
  }

  evento(eventoObj : any) {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      clearTimeout(this.timeLogout);
      this.navCtrl.push(LoginPage);
    }else{
      clearTimeout(this.timeLogout);
      if(eventoObj.tipoProjeto === "DOE" || eventoObj.tipoProjeto === "MISSAO") {
        this.navCtrl.push(MissoesPage);
        this.auth.saveEventInfo(eventoObj);
      }
      else {
        this.navCtrl.push(EventosPage);
        this.auth.saveEventInfo(eventoObj); 
      }
        
      
      
    }
    
  }

  cristolandia() {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      this.navCtrl.push(LoginPage);
    }else{
      this.navCtrl.push(CristolandiaPage);
    }
  }

  zonasul() {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      this.navCtrl.push(LoginPage);
    }else{
      this.navCtrl.push(ZonasulPage);
    }
  }

  gentegrande() {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      this.navCtrl.push(LoginPage);
    }else{
      this.navCtrl.push(GentegrandePage);
    }
  }

  igrejasdosul() {
    let logged = this.auth.getUserInfo();
    if(logged == undefined){
      this.navCtrl.push(LoginPage);
    }else{
      this.navCtrl.push(IgrejasdosulPage);
    }
  }

public currentUser = this.auth.getUserInfo();

}
