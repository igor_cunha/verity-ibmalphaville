import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CadastrarPage } from '../cadastrar/cadastrar';
import { AlertController, LoadingController } from 'ionic-angular';
import { DoePage } from '../doe/doe';
import { MissoesPage } from '../missoes/missoes';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';
import { EsqueciSenhaPage } from '../esqueci-senha/esqueci-senha';
import { InicioPage } from '../inicio/inicio';
import { HttpClient } from '@angular/common/http';
import { AuthServiceProvider, User } from '../../providers/auth-service/auth-service';
import * as $ from "jquery";
import { SyncAsync } from '@angular/compiler/src/util';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  value : any = true;

  loader;
  constructor(private ambiente : AmbientesProvider, public loadingController: LoadingController, public formBuilder: FormBuilder, public nav: NavController, private storage: Storage, private auth : AuthServiceProvider, public alerCtrl: AlertController, public http : HttpClient, public navCtrl: NavController, public navParams: NavParams) {

  }

  

  ionViewDidLoad() {
  }



  cadastrar() {
    this.navCtrl.push(CadastrarPage);
  }

  doe() {
    this.navCtrl.push(DoePage);
  }

  missoes() {
    this.navCtrl.push(MissoesPage);
  }

  cartao() {
    this.navCtrl.push(CadastrarCartaoPage);
  }

  esqueciSenha() {
    this.navCtrl.push(EsqueciSenhaPage);
  }

  inicial() {
    this.navCtrl.push(InicioPage);
  }
 
  private API_URL = this.ambiente.urlBase+'login';
  public email:string = "";
  public senha:string = "";

  public type = 'password';
  public showPass = false;
  public valid: boolean = true;
    ionViewWillLoad() {
      this.auth.logOut();
    if(this.email !== "" && this.senha !== undefined && this.senha !== "") {
      this.valid === true;
    } else {
      this.valid === false;
    }
  }
 
  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  
  entrar(email: string, password: string) {
    if(email !== "" && password !== undefined && password !== "") {
        this.showLoading();
        return new Promise((resolve, reject) => {
          var usuario = {
            email: email,
            password: password
          };
     
          this.http.post(this.API_URL, usuario)
            .subscribe((result: any) => {
              let u = new User(result);
              this.auth.register(u);
              this.storage.set('name', result.name);
              this.storage.set('cpf', result.cpf);
              this.storage.set('phone', result.phone);
              this.storage.set('email', result.email);
              this.dismissLoading();
              this.navCtrl.popToRoot();
            },
            (error) => {
              this.dismissLoading();
              if(error.status === 400) {
                let alert = this.alerCtrl.create({
                  title: 'Ooops...',
                  message: 'Usuário ou senha inválidos!',
                  buttons: [
                    {
                      text: 'Ok',
                      handler: () => {
                        
                      }
                    }
                  ]
                });
                alert.present()
              } else {
                let alert = this.alerCtrl.create({
                  title: 'Ooops...',
                  message: 'Algo de errado ocorreu. Tente de novo mais tarde.',
                  buttons: [
                    {
                      text: 'Ok',
                      handler: () => {
                        
                      }
                    }
                  ]
                  
                });
                alert.present()
              }
            });
        });
    }
    
    
  }
  showLoading() {
    if(!this.loader){
        this.loader = this.loadingController.create({
            content: 'carregando...'
        });
        this.loader.present();
    }
  }
  dismissLoading(){
    if(this.loader){
        this.loader.dismiss();
        this.loader = null;
    }
  }
}