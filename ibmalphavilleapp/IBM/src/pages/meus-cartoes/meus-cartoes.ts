import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';
import { MinhaContaPage } from '../minha-conta/minha-conta';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';

/**
 * Generated class for the MeusCartoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meus-cartoes',
  templateUrl: 'meus-cartoes.html',
})
export class MeusCartoesPage {

  loader;
  constructor(private ambiente : AmbientesProvider, public loadingController: LoadingController, private auth : AuthServiceProvider, public http : HttpClient, public alerCtrl: AlertController ,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  public showCard = false;

  chooseCard() {
    this.showCard = true;
  }
  
  
  public hash : string;
  public list : any;

  cadastrarCartao() {
    this.navCtrl.push(CadastrarCartaoPage);
  }

  ionViewWillEnter() {
    this.hash = this.auth.getUserInfo().hash;
    this.callCards(this.hash);
    
  }

  callCards(hash:string) {
    this.showLoading();
    return new Promise(resolve => {
        this.http.get(this.ambiente.urlBase+"cartao/me/"+hash).subscribe(data => {
        this.list = data;
        this.dismissLoading();
        }, err => {
          this.list = undefined;
          this.dismissLoading();
          if(err.status == '401'){
            this.auth.logOut();
            this.navCtrl.popToRoot();
          }
        });
      });
  }
  showLoading() {
    if(!this.loader){
        this.loader = this.loadingController.create({
            content: 'carregando...'
        });
        this.loader.present();
    }
  }
  dismissLoading(){
    if(this.loader){
        this.loader.dismiss();
        this.loader = null;
    }
  }
  deleteCard(code:number) {
    let confirm = this.alerCtrl.create({
      title: 'Deletar cartão?',
      message: 'Deseja realmente deletar este cartão de seus meios de pagamento?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.showLoading();
            return new Promise(resolve => {
              this.http.delete(this.ambiente.urlBase+"cartao/"+this.hash+"/"+code).subscribe(data => {
                this.callCards(this.hash);
              }, err => {
                this.dismissLoading();
              });
            });
          }
        }
      ]
    });
    confirm.present();
    

  }
}
