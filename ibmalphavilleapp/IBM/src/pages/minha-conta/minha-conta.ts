import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DadosPessoaisPage } from '../dados-pessoais/dados-pessoais';
import { DoacoesMissoesPage } from '../doacoes-missoes/doacoes-missoes';
import { MeusCartoesPage } from '../meus-cartoes/meus-cartoes';
import { PrivacidadePage } from '../privacidade/privacidade';
import { AlertController } from 'ionic-angular';
import { AuthServiceProvider, User } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { PoliticasPage } from '../politicas/politicas';

/**
 * Generated class for the MinhaContaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-minha-conta',
  templateUrl: 'minha-conta.html',
})
export class MinhaContaPage {
  public name : any = "";
 
  
  constructor(private storage: Storage, public alerCtrl: AlertController, private auth : AuthServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
  }

  dadosPessoais() {
    this.navCtrl.push(DadosPessoaisPage);
  }

  doacoesMissoes() {
    this.navCtrl.push(DoacoesMissoesPage);
  }

  meusCartoes() {
    this.navCtrl.push(MeusCartoesPage);
  }

  privacidade() {
    this.navCtrl.push(PrivacidadePage);
  }

  politicas() {
    this.navCtrl.push(PoliticasPage);
  }

  ionViewWillEnter() {
    this.oi();
  }
  

  oi() : any {
    this.storage.get('name').then((val) => {
      this.name = val;
      return val;
    });
  }

  

  logOut() {
    
    let confirm = this.alerCtrl.create({
      title: 'Sair',
      message: 'Quer mesmo sair?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Sair',
          handler: () => {
            this.auth.logOut();
            this.navCtrl.popToRoot();
          }
        }
      ]
    });
    confirm.present()
  }

}
