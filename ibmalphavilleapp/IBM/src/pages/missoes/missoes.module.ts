import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MissoesPage } from './missoes';

@NgModule({
  imports: [
    IonicPageModule.forChild(MissoesPage),
  ],
})
export class MissoesPageModule {}
