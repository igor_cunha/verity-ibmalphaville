import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParabensPage } from './parabens';

@NgModule({
  imports: [
    IonicPageModule.forChild(ParabensPage),
  ],
})
export class ParabensPageModule {}
