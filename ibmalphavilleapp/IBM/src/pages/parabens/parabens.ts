import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';

/**
 * Generated class for the ParabensPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-parabens',
  templateUrl: 'parabens.html',
})
export class ParabensPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  inicio() {
    this.navCtrl.popToRoot();
  }

  cadastrarCartao() {
    this.navCtrl.push(CadastrarCartaoPage);
  }

}
