import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrivacidadePage } from '../privacidade/privacidade';
import { UsoPage } from '../uso/uso';
import { TrocaPage } from '../troca/troca';

/**
 * Generated class for the PoliticasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-politicas',
  templateUrl: 'politicas.html',
})
export class PoliticasPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PoliticasPage');
  }

  privacidade() {
    this.navCtrl.push(PrivacidadePage);
  }

  uso() {
    this.navCtrl.push(UsoPage);
  }

  troca() {
    this.navCtrl.push(TrocaPage);
  }

}
