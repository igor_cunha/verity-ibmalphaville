import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { CadastrarCartaoPage } from '../cadastrar-cartao/cadastrar-cartao';
import { MinhaContaPage } from '../minha-conta/minha-conta';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';

/**
 * Generated class for the TrocaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-troca',
  templateUrl: 'troca.html',
})
export class TrocaPage {

  public html : any;

  constructor(private ambiente : AmbientesProvider, public loadingController: LoadingController, private auth : AuthServiceProvider, public http : HttpClient, public alerCtrl: AlertController ,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrocaPage');
  }

  

}
