import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UsoPage } from './uso';

@NgModule({
  imports: [
    IonicPageModule.forChild(UsoPage),
  ],
})
export class UsoPageModule {}
