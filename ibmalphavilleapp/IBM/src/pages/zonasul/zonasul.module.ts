import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ZonasulPage } from './zonasul';

@NgModule({
  imports: [
    IonicPageModule.forChild(ZonasulPage),
  ],
})
export class ZonasulPageModule {}
