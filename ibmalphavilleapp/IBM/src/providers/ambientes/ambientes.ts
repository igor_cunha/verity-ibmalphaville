import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the AmbientesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AmbientesProvider {

  constructor(public http: HttpClient) {
    console.log('Hello AmbientesProvider Provider');
  }

  //Local
  public urlBase = 'http://localhost:8080/ibm/alphaville/v1/';

  //Homologação
  //public urlBase = 'http://192.168.2.175:8080/ibm/alphaville/v1/';

  //Produção
  //public urlBase ='https://verityibmalphavilleapi.azurewebsites.net/ibm/alphaville/v1/';

}
