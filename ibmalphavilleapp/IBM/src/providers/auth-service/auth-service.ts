import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';


export class User {
  hash:string;
  name:string;
  email:string;
  password:string;
  cpf: string;
  phone: string;
  constructor(data) {
    this.hash = data.hash;
    this.name = data.name;
    this.email = data.email;
    this.cpf = data.cpf;
    this.phone = data.phone;
  }
}

export class Event {
  id : string;
  imagem: string;
  idTipoProjeto: string;
  titulo: string;
  descricao:string;
  valor: number;
  lugar: string;
  endereco: string;
  dataEvento: string;
  horaEvento: string;
  qtdIngresso: number;
  qtdIngressoVendido : number;
  imgLarge : string;
  constructor(data) {
    this.id = data.id;
    this.imagem = data.imagem;
    this.idTipoProjeto = data.tipoProjeto.id;
    this.titulo = data.titulo;
    this.descricao = data.descricao;
    this.valor = data.valor;
    this.lugar = data.lugar;
    this.endereco = data.endereco;
    this.dataEvento = data.dataEvento;
    this.horaEvento = data.horaEvento;
    this.qtdIngresso = data.qtdIngresso;
    this.qtdIngressoVendido = data.qtdIngressoVendido;
    this.imgLarge = data.imgLarge;
  }

}
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  currentUser: User;

  currentEvent : Event;

  constructor(public http: HttpClient) {
  }

  public register(data) {
    this.currentUser = data;
  }

  public logOut() {
    this.currentUser = null;
  }

  public getUserInfo() : User {
    
    return this.currentUser;
  }

  public saveEventInfo(eventObj : Event) : void {
    this.currentEvent = eventObj;
  }

  public getEventInfo() : Event {
    return this.currentEvent;
    
  }


}
