import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController} from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { AmbientesProvider } from '../../providers/ambientes/ambientes';
import { AuthServiceProvider, User} from '../../providers/auth-service/auth-service';
/*
  Generated class for the TransacaoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export class Transacao{
    id : number;
		usuario : any;
		value : string;
		codCard : number;
		transactionDate : string;
		transactionTime : string;
    projeto : any;
    qtdTicket : number
    
    constructor(user:any,value:string,codCard:number,idProjeto:number, qtdTicket?:number){
      this.usuario = {'email':user.email,'password':user.password};
      this.value = value;
      this.codCard = codCard;
      this.projeto = {'id':idProjeto, 'qtdIngresso':qtdTicket};
    }
}

@Injectable()
export class TransacaoProvider {
  loader;
  constructor(private auth : AuthServiceProvider, private ambiente : AmbientesProvider, public loadingController: LoadingController, public http: HttpClient,public alerCtrl: AlertController) {
  }
  public transfer(user:any, value:string, codCard:number, idProjeto:number, qtdTicket?:number){
    this.showLoading();
      let transacao = new Transacao(user,value,codCard,idProjeto, qtdTicket);
      return new Promise((resolve, reject) => {
      this.http.post(this.ambiente.urlBase+"transacao", transacao).subscribe((data: any) => {
        
        let u = new User(data.usuario);
        this.auth.register(u); 

        resolve(data);
        this.dismissLoading();
      }, err => {
        this.dismissLoading();
        let msg:string = 'Algo de errado aconteceu. Tente novamente mais tarde.';
        let title:string = 'Ooops...';
        if(err.status == '400'){
          msg = err.error.message;
          let u = new User(err.error.response.entity);
          this.auth.register(u); 
        }else if(err.status == '403'){
          msg = err.error;
        }
        if(err.status == '401'){
          reject();
        }else{
        this.alerCtrl.create({
              title: title,
              message: msg,
              buttons: [
                {
                  text: 'Ok',
                  handler: () => {
                    
                  }
                }
              ]
            }).present();
        }
      });
    });
      
  }
    public pay(user:any, value:string, codCard:number, idProjeto:number, qtdTicket?:number){
      
  }
  showLoading() {
    if(!this.loader){
        this.loader = this.loadingController.create({
            content: 'carregando...'
        });
        this.loader.present();
    }
  }
  dismissLoading(){
    if(this.loader){
        this.loader.dismiss();
        this.loader = null;
    }
  }
}
