/************************************************************************** 
* Verity TI
* --------------------------- 
* Criado por...:           Igor Almeida
* Em...........:           30/01/2018
* Projeto......:           IBMAlphaville
* Descri��o....:           Script para preenchimento das tabelas do banco IBMAlphaville
* Tabelas envoldidas:      TipoProjeto
**************************************************************************/ 

DECLARE @nomeScript VARCHAR(MAX);
SET @nomeScript = '02. DML IBMAlphaville'
PRINT 'Iniciando execu��o script ['+ @nomeScript +']'
BEGIN TRY
    BEGIN TRANSACTION;
		USE [IBMAlphaville]

		INSERT INTO TipoProjeto VALUES 
										('Doe'),
										('Missão'),
										('Evento')

		INSERT INTO Projeto (idTipoProjeto,titulo, publicado, dataPublicacao) VALUES
			(1, 'Doe', 1, GETDATE()),(2,'Missão',1, GETDATE())
											

		COMMIT TRANSACTION;
 
    PRINT 'Sucesso na execu��o do script ['+ @nomeScript +']'
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			 ROLLBACK TRANSACTION;
             
 		DECLARE @errorNumber INT;
		SET @errorNumber  = ERROR_NUMBER();
		DECLARE @errorLine INT;
		SET @errorLine = ERROR_LINE();
		DECLARE @errorMessage NVARCHAR(4000);
		SET @errorMessage = ERROR_MESSAGE();
		DECLARE @errorSeverity INT;
		SET @errorSeverity = ERROR_SEVERITY();
		DECLARE @errorState INT;
		SET @errorState = ERROR_STATE();
      
		PRINT 'ERRO na execu��o do script. [' + @nomeScript + ']'
		PRINT 'N�mero do erro: [' + CAST(@errorNumber AS VARCHAR(10)) + ']';
		PRINT 'N�mero da linha: [' + CAST(@errorLine AS VARCHAR(10)) + ']';
		PRINT 'Descri��o do erro: [' + @errorMessage + ']';
 
		RAISERROR(@errorMessage, @errorSeverity, @errorState);
END CATCH