package br.com.ibm.alphaville.services;

import java.util.concurrent.Executor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableAsync
public class SpringApp extends AsyncConfigurerSupport {

	/**
	 * Start via Spring Boot
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringApp.class, args);
	}
	@Override
	  public Executor getAsyncExecutor() {
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    executor.setCorePoolSize(2);
	    executor.setMaxPoolSize(2);
	    executor.setQueueCapacity(500);
	    executor.setThreadNamePrefix("send-mailer-");
	    executor.initialize();
	    return executor;
	  }
}
