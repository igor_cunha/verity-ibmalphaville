package br.com.ibm.alphaville.services.bo;

import java.util.List;

import javax.inject.Inject;

import org.json.JSONException;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.CartaoClient;
import br.com.ibm.alphaville.services.enumeration.TipoCartao;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Cartao;
import br.com.ibm.alphaville.services.model.Usuario;
import br.com.ibm.alphaville.services.util.CardLuhnValidator;

/**
 * Classe responsável pelo gerenciamento das regras de negócio relacionadas ao
 * cartão
 * 
 * @author igor.almeida
 *
 */
@Component
public class CartaoBO {

	private static final Character TIPO_CREDITO = 'c';

	@Inject
	private CartaoClient cartaoClient;
	
	public void deleteCard(String hash, Long codCard) {
		cartaoClient.deleteCard(hash, codCard);
	}
	
	public List<Cartao> findCartaoByHash(String hash) throws JSONException {
		List<Cartao> cartoes = cartaoClient.retrieveCards(hash);
		return cartoes;
	}

	public void save(Cartao cartao) throws BusinessException {
		validarCamposObrigatorio(cartao);

		Usuario usuario = cartao.getUsuario();

		// Tira máscara do Número do Cartão
		cartao.setNumeroCartaoFixo(cartao.getNumeroCartao().replace(" ", ""));
		cartao.setMarca(TipoCartao.detect(cartao.getNumeroCartaoFixo()).getSigla());
		// Pega os 4 últimos digitos
		cartao.setUltimosDigitos(cartao.getNumeroCartao().substring(15));
		// Seta NickName
		cartao.setCardNickName("Meu " + cartao.getUltimosDigitos());
		cartao.setTipoCartao(TIPO_CREDITO);

		cartaoClient.createCard(cartao, usuario.getHash());
	}

	private void validarCamposObrigatorio(Cartao cartao) throws BusinessException {
		if (cartao != null) {
			if (cartao.getUsuario() == null || cartao.getUsuario().getHash() == null
					|| cartao.getUsuario().getHash().isEmpty()) {
				throw new BusinessException("O hash do usuário logado é obrigatório.");
			}
			if(cartao.getNumeroCartao() == null || cartao.getNumeroCartao().isEmpty()){
				throw new BusinessException("O campo número do cartão é obrigatório.");
			}else if(!CardLuhnValidator.validate(cartao.getNumeroCartao())){
				throw new BusinessException("Número de cartão inválido.");
			}
			if(cartao.getTitular() == null || cartao.getTitular().isEmpty()){
				throw new BusinessException("O campo titular é obrigatório.");
			}
			if(cartao.getVencimento() == null || cartao.getVencimento().isEmpty()){
				throw new BusinessException("O campo validade é obrigatório.");
			}else if(cartao.getVencimento().length() != 7){
				throw new BusinessException("A data de validade não é válida.");
			}
			if(cartao.getCodSeguranca() == null || cartao.getCodSeguranca().isEmpty()){
				throw new BusinessException("O campo código de segurança é obrigatório.");
			}else if(!cartao.getCodSeguranca().matches("^[0-9]{3,4}$")){
				throw new BusinessException("O código de segurança não é válido.");
			}
		}

	}

}
