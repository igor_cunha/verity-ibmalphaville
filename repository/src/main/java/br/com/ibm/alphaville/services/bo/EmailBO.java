package br.com.ibm.alphaville.services.bo;



import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.model.Mail;
import br.com.ibm.alphaville.services.model.Projeto;
import br.com.ibm.alphaville.services.model.Usuario;
import br.com.ibm.alphaville.services.util.DataUtil;
/**
 * 
 * @author igor.almeida
 *
 */
@Component
public class EmailBO {
	@Autowired
	private JavaMailSender emailSender;
	
	@Value("${spring.mail.username}")
	private String from;
	/**
	 * Método para enviar e-mail com anexo
	 * @param mail Objeto Mail que contem o conteudo do E-mail
	 * @param anexos 
	 * @throws MessagingException
	 */
	public void sendWithAttachments(Mail mail, List<byte[]> anexos) throws MessagingException {

		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
		
		helper.setSubject(mail.getSubject());
		helper.setText(mail.getContent(), true);
		helper.setTo(mail.getTo());
		helper.setFrom(mail.getFrom());
		helper.addInline("logo", new ClassPathResource(File.separator+"img"+File.separator+"logo.png"));
		for (byte[] bs : anexos) {
			helper.addAttachment("Ingresso-IBMAlphaville.pdf", new ByteArrayResource(bs));
		}
		emailSender.send(message);
	}
	/**
	 * Método para enviar e-mail com anexo
	 * @param destinatario Usuario que receberá o e-mail
	 * @param anexo array de byte
	 * @throws MessagingException
	 */
	public void sendWithAttachments(Projeto evento, Usuario destinatario, List<byte[]> anexos) throws MessagingException {
		Mail mail = new Mail();
		BigDecimal valorTotal = evento.getValor().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(anexos.size()));
		String valorString = valorTotal.toString();
				
		mail.setTo(destinatario.getEmail().trim());
		mail.setContent("<div>" + 
				"        <table class=\"m_1555916238119941991MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"width:100.0%;background:#ffffff \">\r\n" + 
				"        <tbody>" + 
				"        <tr>" + 
				"        <td style=\"padding:6.0pt 6.0pt 6.0pt 6.0pt\">" + 
				"        <div align=\"center\">" + 
				"        <table class=\"m_1555916238119941991MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" style=\"width:450.0pt\">\r\n" + 
				"        <tbody>" + 
				"        <tr>" + 
				"        <td valign=\"bottom\" style=\"padding:0cm 0cm 0cm 0cm\">" + 
				"        </td>" + 
				"        </tr>" + 
				"        </tbody>" + 
				"        </table>" + 
				"        </div>" + 
				"        <div align=\"center\">" + 
				"        <table class=\"m_1555916238119941991MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" style=\"width:450.0pt\" id=\"m_1555916238119941991m_6829204622709647021newsletter\">\r\n" + 
				"        <tbody>" + 
				"        <tr>" + 
				"        <td></td>" + 
				"        <td width=\"476\" valign=\"top\" style=\"width:357.0pt;background:white;padding:0cm 27.0pt 0cm 27.0pt\">\r\n" + 
				"        <table class=\"m_1555916238119941991MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"476\" style=\"width:357.0pt\" id=\"m_1555916238119941991m_6829204622709647021content\">\r\n" + 
				"        <tbody>" + 
				"        <tr>" + 
				"        <td style=\"padding:0cm 0cm 0cm 0cm\">" + 
				"        <p class=\"MsoNormal\"><span style=\"font-size:19.5pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\">Olá "+destinatario.getName()+"</span><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\">\r\n" + 
				"        <br>" + 
				"        <br>" + 
				"        <u></u><u></u></span></p>\r\n" + 
				"        <p><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Segue anexo neste e-mail o PDF com o ingresso para ter acesso ao evento <b>"+evento.getTitulo()+"</b>.<u></u><u></u></span></p>\r\n" + 
				"        <div>\r\n" + 
				"        <p class=\"MsoNormal\" style=\"margin-bottom:12.0pt\"><b><span style=\"font-size:10.5pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Informações da compra:</span></b><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\"><br>\r\n" + 
				"        <br>\r\n" + 
				"        </span><b><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Valor total da compra:</span></b><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\"> R$ "+valorString.replace(".", ",")+"</span><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\"><br>\r\n" + 
				"        </div>\r\n" + 
				"        </span><b><span style=\"font-size:10.5pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Informações sobre o evento:</span></b><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\">\r\n<br/>" + 
				"                <br>\r\n" +
				"            </span><b><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Data e horário:</span></b><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\"> "+DataUtil.localDateToDiaSemanaEDataEscrito(evento.getDataEvento())+" | "+evento.getHoraEvento()+"</span><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\"><br>\r\n" + 
				"            </span><b><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Local:</span></b><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\"> "+evento.getLugar()+" | "+evento.getEndereco()+"</span><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\"><br>\r\n" + 
				"            \r\n" + 
				"            <u></u><u></u></span></p>\r\n" + 
				"        <p><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\">Para acessar mais informações, acesse nosso site:<br>\r\n" + 
				"        <a href=\"http://www.ibmalphaville.com.br\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?q=http://ibmalphaville.com.br;source=gmail&amp;ust=1520530796748000&amp;usg=AFQjCNEI6UobWtDKItqrUlQxE34zA5NESw\"><span style=\"color:#3c9acc;text-decoration:none\">www.ibmalphaville.com.br/</span></a>\r\n" + 
				"     \r\n" + 
				"        <p class=\"MsoNormal\" style=\"margin-bottom:12.0pt\"><span style=\"font-size:9.0pt;font-family:&quot;Arial Rounded MT Bold&quot;,sans-serif;color:#787975\"><u></u>&nbsp;<u></u></span></p>\r\n" + 
				"        <table class=\"m_1555916238119941991MsoNormalTable\" border=\"0\" cellspacing=\"3\" cellpadding=\"0\">\r\n" + 
				"        <tbody>\r\n" + 
				"        <tr>\r\n" + 
				"        <td style=\"padding:.75pt .75pt .75pt .75pt\">\r\n" + 
				"        <p class=\"MsoNormal\"><span style=\"font-size:9.0pt;font-family:&quot;Helvetica&quot;,sans-serif;color:#787975\"><b>Deus abençoe.</b><u></u><u></u></span></p>\r\n" + 
				"        </td>\r\n" + 
				"        <td style=\"padding:.75pt .75pt .75pt .75pt\"></td>\r\n" + 
				"        <td style=\"padding:.75pt .75pt .75pt .75pt\">\r\n" + 
				"        </td>\r\n" + 
				"        </tr>\r\n" + 
				"        </tbody>\r\n" + 
				"        </table>\r\n" + 
				"        </td>\r\n" + 
				"        </tr>\r\n" + 
				"        </tbody>\r\n" + 
				"        </table>\r\n" + 
				"        </td>\r\n" + 
				"        <td width=\"25\" valign=\"top\" style=\"width:18.75pt;padding:0cm 0cm 0cm 0cm\"></td>\r\n" + 
				"        </tr>\r\n" + 
				"        </tbody>\r\n" + 
				"        </table>\r\n" + 
				"        </div>\r\n" + 
				"        <div align=\"center\">\r\n" + 
				"        <table class=\"m_1555916238119941991MsoNormalTable\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\" style=\"width:450.0pt\">\r\n" + 
				"        <tbody>\r\n" + 
				"        <tr>\r\n" + 
				"        <td valign=\"bottom\" style=\"padding:0cm 0cm 0cm 0cm\">\r\n" + 
				"        <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><a href=\"http://www.ibmalphaville.com.br\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?q=http://www.ibmalphaville.com.br;source=gmail&amp;ust=1520530796748000&amp;usg=AFQjCNEEmxUA9T5U7rOhaxJni_3gkd4W3Q\"><span style=\"text-decoration:none\"><img border=\"0\" id=\"m_1555916238119941991_x0000_i1030\" src=\"cid:logo\" class=\"CToWUd\"></span></a><u></u><u></u></p>\r\n" + 
				"        </td>\r\n" + 
				"        </tr>\r\n" + 
				"        </tbody>\r\n" + 
				"        </table>\r\n" + 
				"        </div>\r\n" + 
				"        <p class=\"MsoNormal\" align=\"center\" style=\"margin-bottom:12.0pt;text-align:center\">\r\n" + 
				"        <br>\r\n" + 
				"        <br>\r\n" + 
				"        <u></u><u></u></p>\r\n" + 
				"        </td>\r\n" + 
				"        </tr>\r\n" + 
				"        </tbody>\r\n" + 
				"        </table>\r\n" + 
				"        <p class=\"MsoNormal\"><img border=\"0\" width=\"1\" height=\"1\" style=\"width:.0083in;height:.0083in\" id=\"m_1555916238119941991_x0000_i1031\" src=\"https://ci4.googleusercontent.com/proxy/z6-iptrv5H_CRd9UWMP5oIBnFUYTFXrcccWJzHNnd7816QGTMpsCspQSWGGe5oPmx6vV2WVG5_11_dQrVDIITnRpnCmqZZAZf7ZMupeO_bkNk0pK_n4sVewFGIvm74c983X4MppKuOL9Vxx95MRiYhl9IZVDtfPBJj1KJI1hpave1kXshPEeCjyaJ0quqI9gAGDlwu0lk9fPks2HQ7jsArO0CDWRZr_LtsFlVqYfhyEvpaei_HB7za90iPOJgf9qb0cr4gqjmxCN4ZuA3KjjD5RHmDLYyhl9KYJUNpyOh-mjvFPfrWU-TrZgVKLVxTH7pKf7PzpNB6VQN77sN79d3povHwQw_GDI8vvbGT9_M3duySgq_BgqIROBg8BehyNtLnhdMI5IqjU42T8eN0TiGZeAQj6ngJUd9I3UpMxO9lpNtLaLkcY0Iwcifm4iXSAErVze_md0hcz65z8yBihQ6k0MgKdjm3iTQ3lRMT6JrAaFoygvH55XXYw88Ha8hgf-FEDqw0u9zd1vupSInbHc_9sbN7WLPeRMe2zoBzlmubj67Pza_zKdt9Y2Gwj4VoIknM9ni8tlUKY1gkGI1c9tUEx4i3h0YGJtHMurAd-r6OWIY6VKqKAvnWcgIyqwkOEz9wZZmu5RCPSrt4YxzRDpu9nR6pGG_pfZYfDyQQV3u5e-tcQQvMvWqPLYojKL2hQIjSA=s0-d-e1-ft#http://email.sympla.com.br/wf/open?upn=wr-2BO7i72HkLnnd2ZHQvI0mr86twwQlMxvCYLz5FHQGIQmAvOKKZR3EP2okZlFFt03Q5KiignOFi9avLqCM5aMr-2FvUfkDCBmYp7-2FwflXNxlf7FnGyoe0fzfTyfkwvKxGdqryQOvHTuEdUGC-2BejQJvR7PYAUSSM7Dl2uYgeSHuA0tyYRKdHF-2F4ANktwIx64Wwo-2F2CV86c-2FSaw3R-2FHPvLJ7j-2FIHQmMNd6VEMbixRcv71P6KPDviop2VJA1ebuD6KY3zkXzuz5S-2FsaH-2FUomg2PgjZuHY-2BipRPOQdFyYch8fF-2Fc-2F7MzWoBoBIlUHuyzPmWYjKY4c-2FKiae-2BmuyYzeIwEYZz45r-2F44lZNEzk9L5SyM5HQarzWhDCqdRZew368zjyqgN3p4bhUWOvlfQZQVsVof7QlbRH-2F2Zqvz9Sk-2FydT8V0Jo-3D\" class=\"CToWUd\"><u></u><u></u></p>\r\n" + 
				"        </div>");
		mail.setSubject("IBMAlphaville - seus ingressos chegaram!");
		mail.setFrom(from);
		sendWithAttachments(mail, anexos);
		
	}
}
