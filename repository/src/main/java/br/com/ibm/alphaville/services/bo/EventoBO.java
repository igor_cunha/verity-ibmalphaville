package br.com.ibm.alphaville.services.bo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Evento;
import br.com.ibm.alphaville.services.repository.EventoRepository;

/**
 * Classe responsável pelo gerenciamento das regras de negócio relacionadas a eventos
 * @author amauri.souza
 *
 */
//@Component
public class EventoBO {/*

	@Inject
	private EventoRepository repository;
	
	public List<Evento> findByData(String data) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return repository.findByData(sdf.parse(data));
	}
	
	public Evento save(Evento evento) {
		return repository.save(evento);
	}
	
	public Evento findOne(Long id) {
		return repository.findOne(id);
	}
	
	public Iterable<Evento> findAll() {
		return repository.findAll();
	}
	
	public Evento update(Evento evento) throws BusinessException {
		if (repository.exists(evento.getId())) {
			evento = repository.save(evento);
			return evento;
		} else {
			throw new BusinessException("Evento não localizado!");
		}
	}
	
	public void delete(Long id) throws BusinessException {
		Evento evento = this.findOne(id);
		if (evento != null) {
			repository.delete(evento);
		} else {
			throw new BusinessException("Evento não localizado!");
		}
	}
	
*/}
