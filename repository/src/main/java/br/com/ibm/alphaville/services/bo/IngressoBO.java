package br.com.ibm.alphaville.services.bo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.json.JSONException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import br.com.ibm.alphaville.services.model.Ingresso;
import br.com.ibm.alphaville.services.model.Projeto;
import br.com.ibm.alphaville.services.model.Usuario;
import br.com.ibm.alphaville.services.repository.IngressoRepository;
import br.com.ibm.alphaville.services.util.pdf.IngressoTemplate;

@Component
public class IngressoBO {

	@Inject
	private IngressoRepository ingressoRepository;

	@Inject
	private EmailBO emailBO;

	public byte[] gerador(Projeto evento, Long idTransacao, int qtdIngresso)
			throws DocumentException, IOException, MessagingException {
		Ingresso entity = ingressoRepository.save(new Ingresso(idTransacao));
		IngressoTemplate infoTemplate = criarIngressoTemplate(evento, entity);
		byte[] ingressoByte = gerarFile(infoTemplate);

		return ingressoByte;
	}

	private byte[] gerarFile(IngressoTemplate infoTemplate) throws DocumentException, IOException {
		final String SAGOE_UI_BOLD = File.separator + "fonts" + File.separator + "SegoeUIBold.ttf";
		final String SAGOE_UI_SEMI_BOLD = File.separator + "fonts" + File.separator + "segoeuisemibold2.ttf";
		// Create PdfReader instance.
		PdfReader templatePDF = new PdfReader(File.separator + "pdf" + File.separator + "ingresso-template.pdf");

		// Create PdfStamper instance.
		ByteArrayOutputStream ingressoByte = new ByteArrayOutputStream();
		PdfStamper pdfStamper = new PdfStamper(templatePDF, ingressoByte);

		// Create BaseFont instance.
		BaseFont fontSagoeBold = BaseFont.createFont(SAGOE_UI_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);
		BaseFont fontSagoeSemiBold = BaseFont.createFont(SAGOE_UI_SEMI_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);

		// Contain the pdf data.
		PdfContentByte pageContentByte = pdfStamper.getOverContent(1);

		pageContentByte.beginText();

		// TITULO
		pageContentByte.setFontAndSize(fontSagoeBold, 22);
		pageContentByte.setTextMatrix(53, 774);
		pageContentByte.showText(infoTemplate.getTitulo().toUpperCase());

		// Valor
		pageContentByte.setFontAndSize(fontSagoeSemiBold, 18);
		pageContentByte.setTextMatrix(53, 750);
		pageContentByte.showText(infoTemplate.getValor().toUpperCase());

		// Local
		pageContentByte.setFontAndSize(fontSagoeBold, 18);
		pageContentByte.setTextMatrix(53, 700);
		pageContentByte.showText(infoTemplate.getLocal());

		// Endereco
		
		pageContentByte.setFontAndSize(fontSagoeSemiBold, 18);
		if(infoTemplate.getEndereco().length() > 58){
			String[] linesEndereco = infoTemplate.getEndereco().split("\\s-\\s",2);
			
			pageContentByte.setTextMatrix(53, 678);
			pageContentByte.showText(linesEndereco[0]);
			
			pageContentByte.setTextMatrix(53, 661);
			pageContentByte.showText(linesEndereco[1]);
		}else{
			pageContentByte.setTextMatrix(53, 678);
			pageContentByte.showText(infoTemplate.getEndereco());
		}
		
		

		// DiaSemana
		pageContentByte.setFontAndSize(fontSagoeSemiBold, 20);
		pageContentByte.setTextMatrix(53, 520);
		pageContentByte.showText(infoTemplate.getDiaSemana());

		// Data
		pageContentByte.setFontAndSize(fontSagoeSemiBold, 20);
		pageContentByte.setTextMatrix(53, 495);
		pageContentByte.showText(infoTemplate.getData());

		// Codigo
		pageContentByte.setFontAndSize(fontSagoeSemiBold, 20);
		pageContentByte.setTextMatrix(445, 510);
		pageContentByte.showText(infoTemplate.getCodigo());

		pageContentByte.endText();

		// Close the pdfStamper.
		pdfStamper.close();

		return ingressoByte.toByteArray();
	}

	private IngressoTemplate criarIngressoTemplate(Projeto evento, Ingresso ingresso) {

		IngressoTemplate infoTemplate = new IngressoTemplate(evento.getTitulo(), evento.getValor(), evento.getLugar(),
				evento.getEndereco(), evento.getDataEvento(), evento.getHoraEvento(), ingresso.getId());

		return infoTemplate;
	}

	@Async
	public void enviarIngresso(Projeto eventoUpdate, Long idTransacao, int qtdIngresso, Usuario usuario)
			throws MessagingException, JSONException, DocumentException, IOException {
		List<byte[]> bs = new ArrayList<>();
		for (int i = 0; i < qtdIngresso; i++) {
			bs.add(gerador(eventoUpdate, idTransacao, qtdIngresso));
		}
		emailBO.sendWithAttachments(eventoUpdate, usuario, bs);
	}

}
