package br.com.ibm.alphaville.services.bo;

import javax.inject.Inject;

import org.json.JSONException;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.LoginClient;
import br.com.ibm.alphaville.services.model.Usuario;

@Component
public class LoginBO {
	@Inject
	private UsuarioBO usuarioBO;
	
	@Inject
	private LoginClient loginClient;
	
	public Usuario login(Usuario usuario) throws JSONException {
		String retorno = loginClient.signIn(usuario);

		// Adicionando hash no objeto Usuario
		usuario.setHash(retorno);

		Usuario usuarioPreenchido = null;

		usuarioPreenchido = usuarioBO.findUsuarioByHash(usuario.getHash());

		// Retorno
		return usuarioPreenchido;
	}

	public void recoverPassword(String email) {
		loginClient.recoverPassword(email);
	}
}
