package br.com.ibm.alphaville.services.bo;

import java.util.List;

import javax.inject.Inject;

//import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.enumeration.TipoProjeto;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Projeto;
import br.com.ibm.alphaville.services.repository.ProjetoRepository;

@Component
public class ProjetoBO {

	@Inject
	private ProjetoRepository projetoRepository;
	
	public List<Projeto> findPublishedEvents() {
		List<Projeto> projeto = projetoRepository.findByAtivoIsTrueAndPublicadoIsTrueOrderByTipoProjeto();
		return projeto;
	}
	
	public boolean isEvento(Projeto projeto) {
		if (projeto != null && projeto.getTipoProjeto() != null) {
			return projeto.getTipoProjeto().getId().equals(TipoProjeto.EVENTO.getId());
		}
		return false;
	}

	public boolean ingressoDisponivel(Projeto product, Projeto evento) {
		if (product != null && product.getQtdIngresso() != null && product.getQtdIngresso() > 0) {
			if(evento.getQtdIngressoVendido() < evento.getQtdIngresso()){
				return product.getQtdIngresso()+evento.getQtdIngressoVendido() <= evento.getQtdIngresso();
			}
		}
		return false;
	}

	public Projeto getProjetoById(Long id) throws BusinessException {
		Projeto projeto = null;
		if (id != null) {
			projeto = projetoRepository.findById(id);
		}
		if (projeto == null) {
			throw new BusinessException("Projeto não encontrado");
		}
		return projeto;
	}
	//TODO trazer Projeto com apenas com um estoque na lista
	public Projeto validarProjetoEvento(Projeto projeto) throws BusinessException {
		Projeto evento = null;
		Boolean isEvento = null;
		if(projeto != null && projeto.getId() != null){
			evento = getProjetoById(projeto.getId());
			isEvento = isEvento(evento);
			if(isEvento && !ingressoDisponivel(projeto, evento)){
				throw new BusinessException("Não há ingressos disponíveis para este evento");
			}else if(!isEvento){
				return null;
			}
		}
		return evento;
	}
}
