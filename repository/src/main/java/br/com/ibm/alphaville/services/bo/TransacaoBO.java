package br.com.ibm.alphaville.services.bo;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;

import org.json.JSONException;
import org.springframework.stereotype.Component;

import com.itextpdf.text.DocumentException;

import br.com.ibm.alphaville.services.client.TransacaoClient;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Projeto;
import br.com.ibm.alphaville.services.model.Transacao;
import br.com.ibm.alphaville.services.repository.ProjetoRepository;
import br.com.ibm.alphaville.services.repository.TransacaoRepository;
import br.com.ibm.alphaville.services.util.DocumentValidator;

@Component
public class TransacaoBO {

	@Inject
	private TransacaoClient transacaoClient;

	@Inject
	private TransacaoRepository transacaoRepository;

	@Inject
	private ProjetoRepository projetoRepository;

	@Inject
	private ProjetoBO projetoBO;

	@Inject
	private IngressoBO ingressoBO;
	
	@Inject
	private LoginBO loginBO;

	synchronized public Transacao transfer(Transacao transaction) throws BusinessException, NotFoundException,
			JSONException, DocumentException, IOException, MessagingException {
		int qtdIngresso = 0;
		validarCamposObrigatorio(transaction);

		try{
			transaction.setUsuario(loginBO.login(transaction.getUsuario()));
		}catch(BadRequestException e){
			throw new ForbiddenException("Senha inválida");
		}
		Projeto eventoUpdate = projetoBO.validarProjetoEvento(transaction.getProjeto());
		if (eventoUpdate != null) {
			qtdIngresso = transaction.getProjeto().getQtdIngresso();
			transaction.setValue(eventoUpdate.getValor().multiply(new BigDecimal(qtdIngresso)));
			eventoUpdate.setQtdIngressoVendido(
					eventoUpdate.getQtdIngressoVendido() + transaction.getProjeto().getQtdIngresso());
		}
		transaction.setCodTransacao(transacaoClient.prePaidFlow(transaction));

		Long idTransacao = transacaoRepository.save(transaction).getId();
		if (projetoBO.isEvento(eventoUpdate)) {
			projetoRepository.save(eventoUpdate);
			ingressoBO.enviarIngresso(eventoUpdate, idTransacao, qtdIngresso, transaction.getUsuario());
		}

		Transacao receiptTransaction = getReceiptByHashAndCodTransacao(transaction.getUsuario().getHash(),
				transaction.getCodTransacao());
		receiptTransaction.setUsuario(transaction.getUsuario());
		
		return receiptTransaction;
	}

	public List<Transacao> listReceiptByHash(String hash) throws JSONException {
		List<Transacao> transacoes = transacaoClient.findTransacaoByHash(hash);
		for (int i = 0; i < transacoes.size(); i++) {
			Transacao transacaoBanco = transacaoRepository.findByCodTransacao(transacoes.get(i).getCodTransacao());

			if (transacaoBanco != null) {
				transacaoBanco.getProjeto().setImagem(null);
				transacoes.get(i).setId(transacaoBanco.getId());
				transacoes.get(i).setProjeto(transacaoBanco.getProjeto());
			}

		}
		return transacoes;
	}

	public Transacao getReceiptByHashAndCodTransacao(String hash, Long codTransacao) throws JSONException {
		List<Transacao> transacoes = transacaoClient.findTransacaoByHash(hash);
		return transacoes.stream().filter(x -> x.getCodTransacao().equals(codTransacao)).findFirst().orElse(null);
	}

	private void validarCamposObrigatorio(Transacao transaction) throws BusinessException {
		if (transaction != null) {
			if (transaction.getUsuario() == null
					|| !DocumentValidator.preenchido(transaction.getUsuario().getEmail())) {
				throw new BusinessException("O usuário é obrigatório.");
			}
			if (transaction.getCodCard() == null || transaction.getCodCard().longValue() < 1) {
				throw new BusinessException("O campo cartão é obrigatório.");
			}
			if (transaction.getValue() == null || transaction.getValue().doubleValue() <= 0) {
				throw new BusinessException("O campo valor é obrigatório.");
			}
		} else {
			throw new BusinessException("A transação não existe.");
		}
	}
}
