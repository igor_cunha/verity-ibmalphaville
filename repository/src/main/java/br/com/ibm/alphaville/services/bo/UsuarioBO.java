package br.com.ibm.alphaville.services.bo;

import javax.inject.Inject;

import org.json.JSONException;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.UsuarioClient;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Usuario;
import br.com.ibm.alphaville.services.util.DocumentValidator;

@Component
public class UsuarioBO {
	
	@Inject
	private UsuarioClient client;
	
	public Usuario save(Usuario usuario) throws BusinessException{
		validarCamposObrigatorio(usuario);
		
		String hash = client.createAccount(usuario);
		
		//Adicionando hash no objeto Usuario
		usuario.setHash(hash);
		
		//Retorno
		return usuario; 
	}

	private void validarCamposObrigatorio(Usuario usuario) throws BusinessException {
		if(usuario != null){
			if(!DocumentValidator.preenchido(usuario.getName())){
				throw new BusinessException("O campo nome é obrigatório");
			}
			if(!DocumentValidator.preenchido(usuario.getCpf())){
				throw new BusinessException("O campo cpf é obrigatório");
			}else if(!DocumentValidator.isValidCPF(usuario.getCpf())){
				throw new BusinessException("O cpf não é válido.");
			}
			if(!DocumentValidator.preenchido(usuario.getEmail())){
				throw new BusinessException("O campo email é obrigatório");
			}else if(!DocumentValidator.isValidEmail(usuario.getEmail())){
				throw new BusinessException("O email não é válido");
			}
			if(!DocumentValidator.preenchido(usuario.getPhone())){
				throw new BusinessException("O campo telefone é obrigatório");
			}else if(usuario.getPhone().matches("[A-Za-z]+") || usuario.getPhone().length() < 10){
				throw new BusinessException("O telefone não é válido");
			}
			if(!DocumentValidator.preenchido(usuario.getPassword())){
				throw new BusinessException("O campo senha é obrigatório");
			}
		}else{
			throw new BusinessException("Não existe um usuário");
		}
	}

	public Usuario findUsuarioByHash(String hashUser) throws JSONException {
		Usuario usuario = client.findUsuarioByHash(hashUser);
		return usuario;
	}
}
