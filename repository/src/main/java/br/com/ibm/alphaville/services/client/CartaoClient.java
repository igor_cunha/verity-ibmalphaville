package br.com.ibm.alphaville.services.client;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.json.JSONDataParser;
import br.com.ibm.alphaville.services.model.Cartao;

@Component
public class CartaoClient {

	private Client client = ClientBuilder.newClient();
	
	@Value("${url-service-isyBuy}")
	private String urlServiceIsyBuy;
	
	@Value("${service-retrieve-cards-isyBuy}")
	private String serviceRetrieveCardsIsyBuy;
	
	@Value("${service-create-cards-isyBuy}")
	private String serviceCreateCardsIsyBuy;
	
	@Value("${service-delete-card-isyBuy}")
	private String serviceDeleteCardIsyBuy;
	
	@Inject
	private ClientUtil clientUtil;
	
	public void deleteCard(String hash, Long codCard) {
		Entity<Form> entity = clientUtil.createForm(codCard, hash, serviceDeleteCardIsyBuy);
		client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
	}

	public List<Cartao> retrieveCards(String hash) throws JSONException {
		Entity<Form> entity = clientUtil.createForm(hash, hash, serviceRetrieveCardsIsyBuy);
        Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
        
        String stringJson = response.readEntity(String.class);
        if(stringJson.equals("dl")){
        	throw new NotAuthorizedException(Response.status(401));
        }
        List<Cartao> cartoes = JSONDataParser.parseStringJsonToCartaoCollection(stringJson);
		return cartoes;
	}

	/**
	 * @param cartao
	 * @param hash
	 * @return
	 */
	public void createCard(Cartao cartao, String hash) {
		Entity<Form> entity = clientUtil.createForm(cartao, hash, serviceCreateCardsIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		String code = response.readEntity(String.class);
		
		if(code.equals("e")){
			throw new BadRequestException("Senha inválida.");
		}else if(code.equals("NotApproved")){
			throw new BadRequestException("Cartão não aprovado.");
		}else if(code.equals("dl")){
			throw new NotAuthorizedException(Response.status(401));
		}
	}
}
