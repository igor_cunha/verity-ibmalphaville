package br.com.ibm.alphaville.services.client;

import java.security.InvalidParameterException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.model.Cartao;
import br.com.ibm.alphaville.services.model.Transacao;
import br.com.ibm.alphaville.services.model.Usuario;

@Component
public class ClientUtil {
	private static final String USER_HASH = "UserHash";
	private static final String SOFTWARE_VERSION = "softwareversion";
	private static final String SOFTWARE_NAME = "softwarename";
	private static final String SERVICE_TO_BE_CALLED = "ServiceToBeCalled";
	private static final String CONTENT_JSON = "ContentJSON";

	@Value("${isyBuy-version}")
	private String isyBuyVersion;

	@Value("${client-name}")
	private String clientName;
	
	@Value("${client-id}")
	private String clientId;

	@Value("${service-create-accountu-isyBuy}")
	private String serviceCreateAccountuIsyBuy;

	@Value("${service-pre-paid-flow-isyBuy}")
	private String servicePrePaidFlowIsyBuy;

	@Value("${service-retrieve-account-data-isyBuy}")
	private String serviceRetrieveAccountDataIsyBuy;

	@Value("${service-retrieve-cards-isyBuy}")
	private String serviceRetrieveCardsIsyBuy;

	@Value("${service-create-cards-isyBuy}")
	private String serviceCreateCardsIsyBuy;

	@Value("${service-recover-password-isyBuy}")
	private String serviceRecoverPasswordIsyBuy;

	@Value("${service-receipt-isyBuy}")
	private String serviceReceiptIsyBuy;

	@Value("${service-delete-card-isyBuy}")
	private String serviceDeleteCardIsyBuy;

	@Value("${service-login-isyBuy}")
	private String serviceLoginIsyBuy;
	
	@Value("${service-user-terms-isyBuy}")
	private String serviceUserTermsIsyBuy;
	
	@Value("${service-privacy-policy-isyBuy}")
	private String servicePrivacyPolicyIsyBuy;
	
	@Value("${service-exchange-policy-isyBuy}")
	private String serviceExchangePolicyIsyBuy;

	public Entity<Form> createForm(Object object, String userHash, String serviceToBeCalled) {
		Form form = new Form();

		String contentJSON = createContentJSON(object, serviceToBeCalled);

		form.param(USER_HASH, userHash).param(SOFTWARE_VERSION, isyBuyVersion).param(SOFTWARE_NAME, clientName)
				.param(SERVICE_TO_BE_CALLED, serviceToBeCalled).param(CONTENT_JSON, contentJSON);
		Entity<Form> entity = Entity.form(form);

		return entity;

	}

	private String createContentJSON(Object object, String serviceToBeCalled) {
		StringBuilder contentJSON = new StringBuilder();

		if (serviceToBeCalled.equals(serviceCreateAccountuIsyBuy)) {
			return createContentJSONCreateAccount(object, contentJSON);
		} else if (serviceToBeCalled.equals(servicePrePaidFlowIsyBuy)) {
			return createContentJSONPrePaidFlow(object, contentJSON);
		} else if (serviceToBeCalled.equals(serviceRetrieveAccountDataIsyBuy)) {
			return "{}";
		} else if (serviceToBeCalled.equals(serviceRetrieveCardsIsyBuy)) {
			return createContentJSONRetrieveCards(object, contentJSON);
		} else if (serviceToBeCalled.equals(serviceCreateCardsIsyBuy)) {
			return createContentJSONCreateCards(object, contentJSON);
		} else if (serviceToBeCalled.equals(serviceRecoverPasswordIsyBuy)) {
			return createContentJSONPasswordRecovery(object, contentJSON);
		} else if (serviceToBeCalled.equals(serviceReceiptIsyBuy)) {
			return createContentJSONReceipt(object, contentJSON);
		} else if (serviceToBeCalled.equals(serviceDeleteCardIsyBuy)) {
			return createContentJSONDeleteCard(object, contentJSON);
		} else if (serviceToBeCalled.equals(serviceLoginIsyBuy)) {
			return createContentJSONLogin(object, contentJSON);
		}else if(serviceToBeCalled.equals(serviceUserTermsIsyBuy) || 
				serviceToBeCalled.equals(servicePrivacyPolicyIsyBuy) || 
				serviceToBeCalled.equals(serviceExchangePolicyIsyBuy)){
			return createContentJSONContract(object, contentJSON);
		}
		throw new InvalidParameterException("serviceToBeCalled not exist.");
	}

	private String createContentJSONContract(Object object, StringBuilder contentJSON) {
		contentJSON.append(" {}");
		
		return contentJSON.toString();
	}

	private String createContentJSONLogin(Object object, StringBuilder contentJSON) {
		if (object instanceof Usuario) {
			Usuario usuario = (Usuario) object;

			contentJSON.append(" {\"fbconnect\": false, \"loginType\" : \"regular\", ");
			contentJSON.append("\"emaillogin\":\"");
			contentJSON.append(usuario.getEmail());
			contentJSON.append("\",\"passwordlogin\":\"");
			contentJSON.append(usuario.getPassword());
			contentJSON.append("\",\"DeviceType\":\"An\",\"fid\":\"\"}");
		} else {
			throw new InvalidParameterException("First invalid parameter");
		}
		return contentJSON.toString();

	}

	private String createContentJSONReceipt(Object object, StringBuilder contentJSON) {
		if (object instanceof String) {
			String hashUser = (String) object;

			contentJSON.append(" {\"preCodU\":\"");
			contentJSON.append(hashUser);
			contentJSON.append("\",\"maxLocalReceiptNumber\":0}");
		}
		return contentJSON.toString();
	}

	private String createContentJSONCreateCards(Object object, StringBuilder contentJSON) {
		if (object instanceof Cartao) {
			Cartao cartao = (Cartao) object;

			contentJSON.append("{\"CardNickname\":\"");
			contentJSON.append(cartao.getCardNickName());
			contentJSON.append("\",\"brand\":\"");
			contentJSON.append(cartao.getMarca());
			contentJSON.append("\",\"cardnumber\":\"");
			contentJSON.append(cartao.getNumeroCartao());
			contentJSON.append("\",\"goodtomonth\":\"");
			contentJSON.append(cartao.getVencimento().substring(5));
			contentJSON.append("\",\"goodtoyear\":\"");
			contentJSON.append(cartao.getVencimento().substring(0, 4));
			contentJSON.append("\",\"cardname\":\"");
			contentJSON.append(cartao.getTitular());
			contentJSON.append("\",\"csc\":\"");
			contentJSON.append(cartao.getCodSeguranca());
			contentJSON.append("\",\"passwordaccount\":\"");
			contentJSON.append(cartao.getUsuario().getPassword());
			contentJSON.append("\",\"cardnumberfixed\":\"");
			contentJSON.append(cartao.getNumeroCartaoFixo());
			contentJSON.append("\",\"lastnumbers\":\"");
			contentJSON.append(cartao.getUltimosDigitos());
			contentJSON.append("\",\"cardType\":\"");
			contentJSON.append(cartao.getTipoCartao().toString());
			contentJSON.append("\"}");
		}
		return contentJSON.toString();
	}

	private String createContentJSONRetrieveCards(Object object, StringBuilder contentJSON) {
		if (object instanceof String) {
			String hashUser = (String) object;

			contentJSON.append(" {\"preCodU\":\"");
			contentJSON.append(hashUser);
			contentJSON.append("\",\"mytime\":0}");
		}
		return contentJSON.toString();
	}

	private String createContentJSONPrePaidFlow(Object object, StringBuilder contentJSON) {
		if (object instanceof Transacao) {
			Transacao transacao = (Transacao) object;

			contentJSON.append("{\"preCodM\":\""+clientId+"\",\"ServiceType\":\"ARB\",\"amount\":\"");
			contentJSON.append(transacao.getValueIsyBuy().intValue());
			contentJSON.append("\",\"codcard\":\"");
			contentJSON.append(transacao.getCodCard());
			contentJSON.append("\"}");
		} else {
			throw new InvalidParameterException("First invalid parameter");
		}
		return contentJSON.toString();
	}

	private String createContentJSONCreateAccount(Object object, StringBuilder contentJSON) {
		if (object instanceof Usuario) {
			Usuario usuario = (Usuario) object;

			contentJSON.append(" {\"nicknameaccount\":\"");
			contentJSON.append(usuario.getName());
			contentJSON.append("\",\"iddoc\":\"");
			contentJSON.append(usuario.getCpf());
			contentJSON.append("\",\"emailaccount\":\"");
			contentJSON.append(usuario.getEmail());
			contentJSON.append("\",\"phonenumber\":\"");
			contentJSON.append(usuario.getPhone());
			contentJSON.append("\",\"passwordaccount\":\"");
			contentJSON.append(usuario.getPassword());
			contentJSON.append("\",\"facebookid\":\"\",\"DeviceType\":\"An\"}");
		} else {
			throw new InvalidParameterException("First invalid parameter");
		}
		return contentJSON.toString();
	}

	private String createContentJSONPasswordRecovery(Object object, StringBuilder contentJSON) {
		if (object instanceof String) {

			contentJSON.append(" {\"email\":\"");
			contentJSON.append(object);
			contentJSON.append("\"}");

		} else {
			throw new InvalidParameterException("First invalid parameter");
		}
		return contentJSON.toString();
	}

	private String createContentJSONDeleteCard(Object object, StringBuilder contentJSON) {
		if (object instanceof Long) {

			contentJSON.append(" {\"codcard\":\"");
			contentJSON.append(object);
			contentJSON.append("\"}");

		} else {
			throw new InvalidParameterException("First invalid parameter");
		}
		return contentJSON.toString();
	}
}
