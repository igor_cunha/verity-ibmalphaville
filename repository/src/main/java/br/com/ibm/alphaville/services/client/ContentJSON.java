package br.com.ibm.alphaville.services.client;


public class ContentJSON {
	public String userHash;
	public String softwareversion;
	public String softwarename;
	public String serviceToBeCalled;
	public String contentJSON;
	
	public String getUserHash() {
		return userHash;
	}
	public void setUserHash(String userHash) {
		this.userHash = userHash;
	}
	public String getSoftwareversion() {
		return softwareversion;
	}
	public void setSoftwareversion(String softwareversion) {
		this.softwareversion = softwareversion;
	}
	public String getSoftwarename() {
		return softwarename;
	}
	public void setSoftwarename(String softwarename) {
		this.softwarename = softwarename;
	}
	public String getServiceToBeCalled() {
		return serviceToBeCalled;
	}
	public void setServiceToBeCalled(String serviceToBeCalled) {
		this.serviceToBeCalled = serviceToBeCalled;
	}
	public String getContentJSON() {
		return contentJSON;
	}
	public void setContentJSON(String contentJSON) {
		this.contentJSON = contentJSON;
	}
	
	
}
