package br.com.ibm.alphaville.services.client;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.model.Contrato;

@Component
public class ContratoClient {
	
	private Client client = ClientBuilder.newClient();
	
	@Value("${url-service-isyBuy}")
	private String urlServiceIsyBuy;
	
	@Value("${service-user-terms-isyBuy}")
	private String serviceUserTermsIsyBuy;
	
	@Value("${service-privacy-policy-isyBuy}")
	private String servicePrivacyPolicyIsyBuy;
	
	@Value("${service-exchange-policy-isyBuy}")
	private String serviceExchangePolicyIsyBuy;
	
	@Inject
	private ClientUtil clientUtil;
	
	public Contrato getUserTerms() {
		Entity<Form> entity = clientUtil.createForm(null, "", serviceUserTermsIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		
		String retorno = response.readEntity(String.class);
		Contrato contrato = new Contrato();
		contrato.setConteudo(retorno);
		return contrato;
	}
	public Contrato getPrivacyPolicy() {
		Entity<Form> entity = clientUtil.createForm(null, "", servicePrivacyPolicyIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		
		String retorno = response.readEntity(String.class);
		Contrato contrato = new Contrato();
		contrato.setConteudo(retorno);
		return contrato;
	}
	public Contrato getExchangePolicy() {
		Entity<Form> entity = clientUtil.createForm(null, "", serviceExchangePolicyIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		
		String retorno = response.readEntity(String.class);
		Contrato contrato = new Contrato();
		contrato.setConteudo(retorno);
		return contrato;
	}
}
