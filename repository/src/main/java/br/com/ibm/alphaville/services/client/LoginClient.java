package br.com.ibm.alphaville.services.client;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.model.Usuario;

@Component
public class LoginClient {
	Client client = ClientBuilder.newClient();
	
	@Inject
	private ClientUtil clientUtil;
	
	@Value("${url-service-isyBuy}")
	private String urlServiceIsyBuy;
	
	@Value("${service-recover-password-isyBuy}")
	private String serviceRetrieveCardsIsyBuy;
	
	@Value("${service-login-isyBuy}")
	private String serviceLoginIsyBuy;
	
	public void recoverPassword(String email) {
		Entity<Form> entity = clientUtil.createForm(email, "", serviceRetrieveCardsIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		
		String value = response.readEntity(String.class);
		if(value.equals("\"emaildoesntexist\"")) {
			throw new BadRequestException("E-mail não cadastrado");
		}
	}
	public String signIn(Usuario usuario) throws JSONException {
		Entity<Form> entity = clientUtil.createForm(usuario, "", serviceLoginIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);

		String value = response.readEntity(String.class);

		if (value.equals("\"e\"")) {
			throw new BadRequestException("Usuário ou senha inválidos.");
		} else {
			JSONObject jsonObject = new JSONObject(value);
			String loginId = jsonObject.getString("LoginId");

			return loginId;
		}
	}
	
}
