package br.com.ibm.alphaville.services.client;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.json.JSONDataParser;
import br.com.ibm.alphaville.services.client.json.JSONUtil;
import br.com.ibm.alphaville.services.model.Transacao;

@Component
public class TransacaoClient {

	private Client client = ClientBuilder.newClient();

	@Value("${service-pre-paid-flow-isyBuy}")
	private String servicePrePaidFlowIsyBuy;

	@Autowired
	private ClientUtil clientUtil;

	@Value("${url-service-isyBuy}")
	private String urlServiceIsyBuy;

	@Value("${service-receipt-isyBuy}")
	private String serviceReceiptIsyBuy;

	public Long prePaidFlow(Transacao transacao) throws JSONException {
		Entity<Form> entity = clientUtil.createForm(transacao, transacao.getUsuario().getHash(), servicePrePaidFlowIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);

		if (response.getStatus() == 404) {
			throw new NotFoundException();
		}
		String stringJson = response.readEntity(String.class);
		if (JSONUtil.isJSON(stringJson)) {
			return JSONDataParser.parseStringJsonToCodTransacao(stringJson);
		}
		if (stringJson.equals("dl")) {
			throw new NotAuthorizedException(Response.status(401));
		} else {
			throw new BadRequestException("Não foi possível realizar a transferência.", Response.status(400).entity(transacao.getUsuario()).build());
		}
	}

	public List<Transacao> findTransacaoByHash(String hash) throws JSONException {
		Entity<Form> entity = clientUtil.createForm(hash, hash, serviceReceiptIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);

		if (response.getStatus() == 404) {
			throw new NotFoundException();
		}
		List<Transacao> transacoes = new ArrayList<>();
		String jsonString = response.readEntity(String.class);
		if (jsonString.equals("dl")) {
			throw new NotAuthorizedException(Response.status(401));
		}
		try {
			transacoes = JSONDataParser.parseStringJsonToTransacaoCollection(jsonString);
		} catch (JSONException e) {
			return transacoes;
		}
		transacoes.sort(Comparator.comparing(Transacao::getTransactionDate).thenComparing(Transacao::getTransactionTime)
				.reversed());

		return transacoes;
	}
}
