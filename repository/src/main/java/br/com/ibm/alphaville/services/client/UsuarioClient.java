package br.com.ibm.alphaville.services.client;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.json.JSONDataParser;
import br.com.ibm.alphaville.services.client.json.JSONUtil;
import br.com.ibm.alphaville.services.model.Usuario;
import br.com.ibm.alphaville.services.util.DocumentValidator;

@Component
public class UsuarioClient {
	Client client = ClientBuilder.newClient();

	@Autowired
	private ClientUtil clientUtil;

	@Value("${url-service-isyBuy}")
	private String urlServiceIsyBuy;

	@Value("${service-retrieve-account-data-isyBuy}")
	private String serviceRetrieveAccountDataIsyBuy;

	@Value("${service-create-accountu-isyBuy}")
	private String serviceCreateAccountuIsyBuy;

	public String createAccount(Usuario usuario) {
		Entity<Form> entity = clientUtil.createForm(usuario, "", serviceCreateAccountuIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
		String jsonString = response.readEntity(String.class);

		String hash = null;

		if (JSONUtil.isJSON(jsonString)) {
			try {
				hash = JSONDataParser.parseStringJsonToHashString(jsonString);
			} catch (JSONException e) {
				throw new BadRequestException("E-mail já cadastrado em nossa base de dados.");
			}
		}
		if (DocumentValidator.preenchido(hash)) {
			return hash;
		}
		throw new BadRequestException("Verifique suas informações.");
	}

	public Usuario findUsuarioByHash(String hashUser) throws JSONException {
		Entity<Form> entity = clientUtil.createForm(hashUser, hashUser, serviceRetrieveAccountDataIsyBuy);
		Response response = client.target(urlServiceIsyBuy).request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);

		String stringJson = response.readEntity(String.class);

		Usuario usuario = JSONDataParser.parseStringJsonToUsuario(stringJson);
		usuario.setHash(hashUser);

		return usuario;
	}
}
