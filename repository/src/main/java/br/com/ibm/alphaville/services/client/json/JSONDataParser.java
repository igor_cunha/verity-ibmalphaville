package br.com.ibm.alphaville.services.client.json;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.ibm.alphaville.services.model.Cartao;
import br.com.ibm.alphaville.services.model.Transacao;
import br.com.ibm.alphaville.services.model.Usuario;

public class JSONDataParser {
	
	private static final String CARTAO_ATIVO = "a"; 

	public static Usuario parseStringJsonToUsuario(String stringJson) throws JSONException {
		JSONObject jsonObject = new JSONArray(stringJson).getJSONObject(0);

        Usuario usuario = new Usuario();
        
        //usuario.setCodUser(jsonObject.isNull("iddoc")? null:jsonObject.getLong("iddoc"));
        usuario.setCpf(jsonObject.isNull("iddoc")? null:jsonObject.getString("iddoc"));
        usuario.setEmail(jsonObject.getString("emailaccount"));
        usuario.setName(jsonObject.getString("nicknameaccount"));
        usuario.setPhone(jsonObject.isNull("phonenumber")? null:jsonObject.getString("phonenumber"));
		
        return usuario;
	}

	public static List<Cartao> parseStringJsonToCartaoCollection(String stringJson) throws JSONException {
		JSONArray jsonArray = new JSONArray(stringJson);
		List<Cartao> cartoes = new ArrayList<>();
		Cartao cartao = null;
		for(int i = 0; i < jsonArray.length(); i++){
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			cartao = new Cartao();
			
			cartao.setAtivo(jsonObject.get("CardStatus").equals(CARTAO_ATIVO));
			cartao.setCardNickName(jsonObject.getString("CardNickname"));
			cartao.setCodCartao(Long.parseLong(jsonObject.getString("Codcard")));
			cartao.setMarca(jsonObject.getString("Brand"));
			cartao.setUltimosDigitos(jsonObject.getString("lastnumbers"));
			cartao.setVencimento(jsonObject.getString("vencimento"));
			
			cartoes.add(cartao);
		}
		return cartoes;
	}

	public static Long parseStringJsonToCodTransacao(String stringJson) throws JSONException {
		JSONObject jsonObject = new JSONObject(stringJson);
		
		Long codTransacao = jsonObject.getLong("codTable");
		
		return codTransacao;
	}

	public static String parseStringJsonToHashString(String stringJson) throws JSONException {
		JSONObject jsonObject = new JSONObject(stringJson);
		
		String hash = jsonObject.getString("LoginId");
		
		return hash;
	}

	public static List<Transacao> parseStringJsonToTransacaoCollection(String jsonString) throws JSONException {
		JSONArray jsonArray = new JSONArray(jsonString).getJSONArray(0);
		List<Transacao> transacoes = new ArrayList<>();
		Transacao transacao = null;
		for(int i = 0; i < jsonArray.length(); i++){
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			transacao = new Transacao();
			
			transacao.setCodTransacao(jsonObject.getLong("ct"));
			transacao.setValue(new BigDecimal(jsonObject.getDouble("value")/100).setScale(2, RoundingMode.HALF_UP));

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(jsonObject.getString("date"), formatter);
			
			transacao.setCodCheckin(jsonObject.getLong("re"));
			transacao.setTransactionDate(dateTime.toLocalDate());
			transacao.setTransactionTime(dateTime.toLocalTime());
			
			transacoes.add(transacao);
		}
		return transacoes;
	}

}
