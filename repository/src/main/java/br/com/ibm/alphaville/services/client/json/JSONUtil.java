package br.com.ibm.alphaville.services.client.json;

public class JSONUtil {
	
	private final static String PATTERN_JSON = "^\\{.*\\}$|^[.*]$";
	
	public static boolean isJSON(String stringJson) {
		return stringJson.matches(PATTERN_JSON);
	}

}
