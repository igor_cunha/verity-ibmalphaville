package br.com.ibm.alphaville.services.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.resource.CartaoResource;
import br.com.ibm.alphaville.services.resource.ContratoResource;
import br.com.ibm.alphaville.services.resource.LoginResource;
import br.com.ibm.alphaville.services.resource.ProjetoResource;
import br.com.ibm.alphaville.services.resource.TrasacaoResource;
import br.com.ibm.alphaville.services.resource.UsuarioResource;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@ApplicationPath("/ibm/alphaville/v1")
@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		//register(EventoResource.class);
		register(UsuarioResource.class);
		register(WadlResource.class);
		register(ApiListingResource.class);
		register(SwaggerSerializers.class);
		register(CorsFilter.class);
		register(TrasacaoResource.class);
		register(LoginResource.class);
		register(CartaoResource.class);
		register(ProjetoResource.class);
		register(ContratoResource.class);

		BeanConfig conf = new BeanConfig();
		conf.setTitle("IBM Alphaville");
		conf.setDescription(
				"API para integração com os serviços relacionados as Igreja Batista Memorial de Alphaville.");
		conf.setContact("Verity Development Team");
		conf.setLicense("Verity - Copyright © 2017 todos os direitos reservados");
		conf.setVersion("v1");
		conf.setSchemes(new String[] { "http", "https" });
		conf.setResourcePackage("br.com.ibm.alphaville.services");
		conf.setScan(true);
	}

}
