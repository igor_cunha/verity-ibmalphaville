package br.com.ibm.alphaville.services.enumeration;

import java.util.regex.Pattern;

public enum TipoCartao {
	
    UNKNOWN,
    VISA("^4[0-9]{12}(?:[0-9]{3}){0,2}$","v"),
    MASTERCARD("^(?:5[1-5]|2(?!2([01]|20)|7(2[1-9]|3))[2-7])\\d{14}$","m"),
    DINERS_CLUB("^3(?:0[0-5]\\d|095|6\\d{0,2}|[89]\\d{2})\\d{12,15}$","dn"),
    DISCOVER("^6(?:011|[45][0-9]{2})[0-9]{12}$","d");

    private Pattern pattern;
    private String sigla;
    
    TipoCartao() {
        this.pattern = null;
    }

    TipoCartao(String pattern,String sigla) {
        this.pattern = Pattern.compile(pattern);
        this.sigla = sigla;
    }

    public static TipoCartao detect(String cardNumber) {

        for (TipoCartao cardType : TipoCartao.values()) {
            if (null == cardType.pattern) continue;
            if (cardType.pattern.matcher(cardNumber).matches()) return cardType;
        }

        return UNKNOWN;
    }

	public String getSigla() {
		return sigla;
	}
}
