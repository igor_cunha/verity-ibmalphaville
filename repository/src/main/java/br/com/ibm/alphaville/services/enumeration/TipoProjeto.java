package br.com.ibm.alphaville.services.enumeration;

public enum TipoProjeto {
	Unknown,
	DOE(1L),
	MISSAO(2L),
	EVENTO(3L);
	
	private Long id;
	
	TipoProjeto(){
		
	}
	TipoProjeto(Long id){
		this.id= id;
	}

	public Long getId() {
		return id;
	}
}
