package br.com.ibm.alphaville.services.exceptions;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BusinessException(String string) {
		super(string);
	}

}
