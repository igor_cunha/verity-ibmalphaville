package br.com.ibm.alphaville.services.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author igor.almeida
 */
@ApiModel(value="Cartao", description="Entidade que representa um cartão de um usuário do IBM Alphaville")
public class Cartao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long codCartao;
	
	@ApiModelProperty(dataType="String", name="cardNickName", required = true, value = "Apelido do cartão")
	private String cardNickName;
	
	private Character tipoCartao;
	
	private String marca;
	
	private String ultimosDigitos;
	
	@ApiModelProperty(dataType="String", name="vencimanto", required = true, value = "Data de vencimento do cartão")
	private String vencimento;
	
	@ApiModelProperty(dataType="String", name="numeroCartao", required = true, value = "Número do cartão")
	private String numeroCartao;
	
	private String numeroCartaoFixo;
	
	@ApiModelProperty(dataType="String", name="titular", required = true, value = "Titular do cartão")
	private String titular;
	
	@ApiModelProperty(dataType="String", name="codSeguranca", required = true, value = "Código de segurança do cartão")
	private String codSeguranca;
	
	@ApiModelProperty(dataType="Boolean", name="ativo", required = false, value = "Ativo cartão")
	private Boolean ativo;
	
	private Usuario usuario;
	
	public Cartao(){
		
	}
	public Cartao(Long codCartao, String cardNickName, Character tipoCartao, String marca, String ultimosDigitos,
			String vencimento, String numeroCartao, String titular, String codSeguranca, Boolean ativo, Usuario usuario) {
		super();
		this.codCartao = codCartao;
		this.cardNickName = cardNickName;
		this.tipoCartao = tipoCartao;
		this.marca = marca;
		this.ultimosDigitos = ultimosDigitos;
		this.vencimento = vencimento;
		this.numeroCartao = numeroCartao;
		this.titular = titular;
		this.codSeguranca = codSeguranca;
		this.ativo = ativo;
		this.usuario = usuario;
	}

	public Long getCodCartao() {
		return codCartao;
	}

	public void setCodCartao(Long codCartao) {
		this.codCartao = codCartao;
	}

	public String getCardNickName() {
		return cardNickName;
	}

	public void setCardNickName(String cardNickName) {
		this.cardNickName = cardNickName;
	}

	public Character getTipoCartao() {
		return tipoCartao;
	}

	public void setTipoCartao(Character tipoCartao) {
		this.tipoCartao = tipoCartao;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getUltimosDigitos() {
		return ultimosDigitos;
	}

	public void setUltimosDigitos(String ultimosDigitos) {
		this.ultimosDigitos = ultimosDigitos;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public String getNumeroCartaoFixo() {
		return numeroCartaoFixo;
	}
	public void setNumeroCartaoFixo(String numeroCartaoFixo) {
		this.numeroCartaoFixo = numeroCartaoFixo;
	}
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getCodSeguranca() {
		return codSeguranca;
	}

	public void setCodSeguranca(String codSeguranca) {
		this.codSeguranca = codSeguranca;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
