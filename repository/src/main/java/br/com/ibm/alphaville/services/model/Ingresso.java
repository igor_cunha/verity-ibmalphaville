package br.com.ibm.alphaville.services.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * 
 * @author igor.almeida
 *
 */

@Entity
@Table(name="Ingresso")
@NamedQuery(name="Ingresso.findAll", query="SELECT e FROM Ingresso e")
public class Ingresso implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Id")
	private Long id;
	
	@Column(name="idTransacao")
	private Long idTransacao;

	public Ingresso(Long idTransacao) {
		setIdTransacao(idTransacao);
	}
	public Ingresso(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdTransacao() {
		return idTransacao;
	}

	public void setIdTransacao(Long idTransacao) {
		this.idTransacao = idTransacao;
	}
}
