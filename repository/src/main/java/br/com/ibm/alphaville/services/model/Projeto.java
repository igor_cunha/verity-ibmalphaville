package br.com.ibm.alphaville.services.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.ibm.alphaville.services.enumeration.TipoProjeto;
import br.com.ibm.alphaville.services.util.JsonDateDeserializer;
import br.com.ibm.alphaville.services.util.JsonDateSerializer;
import br.com.ibm.alphaville.services.util.JsonHourSerializer;
import io.swagger.annotations.ApiModel;

/**
 * The persistent class for the Transacao database table.
 * 
 */
@Entity
@Table(name="Projeto")
@NamedQuery(name="Projeto.findAll", query="SELECT e FROM Projeto e")
@ApiModel(value="Projeto", description="Entidade que representa um Projeto de IBM Alphaville")
public class Projeto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="idTipoProjeto")
	private TipoProjeto tipoProjeto;
	
	@Column(name="titulo")
	private String titulo;
	
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="linkSite")
	private String linkSite;
	
	@Column(name="img")
	private String imagem;
	
	@Column(name="valor")
	private BigDecimal valor;
	
	@Column(name="qtdIngresso")
	private Integer qtdIngresso;
	
	@Column(name="qtdIngressoVendido")
	private Integer qtdIngressoVendido;
	
	@Column(name="lugar")
	private String lugar;
	
	@Column(name="endereco")
	private String endereco;
	
	@JsonDeserialize(using = JsonDateDeserializer.class)  
	@JsonSerialize(using = JsonDateSerializer.class)
	@Column(name="dataEvento")
	private LocalDate dataEvento;
	 
	@JsonSerialize(using = JsonHourSerializer.class)
	@Column(name="horaEvento")
	private LocalTime horaEvento;
	
	@Column(name="publicado")
	private Boolean publicado;
	
	@Column(name="ativo")
	private Boolean ativo;
	
	@Column(name = "imgLarge")
	private String imgLarge;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLinkSite() {
		return linkSite;
	}

	public void setLinkSite(String linkSite) {
		this.linkSite = linkSite;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getQtdIngresso() {
		return qtdIngresso;
	}

	public void setQtdIngresso(Integer qtdIngresso) {
		this.qtdIngresso = qtdIngresso;
	}

	public Integer getQtdIngressoVendido() {
		return qtdIngressoVendido == null? 0:qtdIngressoVendido;
	}

	public void setQtdIngressoVendido(Integer qtdIngressoVendido) {
		this.qtdIngressoVendido = qtdIngressoVendido;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public LocalDate getDataEvento() {
		return dataEvento;
	}

	public void setDataEvento(LocalDate dataEvento) {
		this.dataEvento = dataEvento;
	}

	public Boolean getPublicado() {
		return publicado;
	}

	public void setPublicado(Boolean publicado) {
		this.publicado = publicado;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProjeto getTipoProjeto() {
		return tipoProjeto;
	}

	public void setTipoProjeto(TipoProjeto tipoProjeto) {
		this.tipoProjeto = tipoProjeto;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public LocalTime getHoraEvento() {
		return horaEvento;
	}

	public void setHoraEvento(LocalTime horaEvento) {
		this.horaEvento = horaEvento;
	}

	
	public String getImgLarge() {
		return imgLarge;
	}

	public void setImgLarge(String imgLarge) {
		this.imgLarge = imgLarge;
	}
}
