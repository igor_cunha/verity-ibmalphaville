package br.com.ibm.alphaville.services.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.ibm.alphaville.services.util.JsonDateDeserializer;
import br.com.ibm.alphaville.services.util.JsonDateSerializer;
import br.com.ibm.alphaville.services.util.JsonMoneySerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * The persistent class for the Transacao database table.
 * 
 */
@Entity
@Table(name="Transacao")
@NamedQuery(name="Transacao.findAll", query="SELECT e FROM Transacao e")
@ApiModel(value="Transacao", description="Entidade que representa uma Transacao da IBM Alphaville")
public class Transacao implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Id")
	private Long id;
	
	@Column(name="codTransacao")
	private Long codTransacao;
	
	@Transient
	@ApiModelProperty(name="usuario", required = true, value = "usuário que realizou a transação")
	private Usuario usuario;
	
	@ApiModelProperty(name="value", required = true, value = "Valor da transação")
	@NotNull
	@Column(name="valor")
	@JsonSerialize(using = JsonMoneySerializer.class) 
	private BigDecimal value;
	
	@Transient
	@ApiModelProperty(dataType="Long", name="codCard", required = true, value = "Cartão utilizado na transação")
	@NotNull
	@Min(1)
	private Long codCard;
	
	@Transient
	private Long codCheckin;
	
	@Transient
	@JsonDeserialize(using = JsonDateDeserializer.class)  
	@JsonSerialize(using = JsonDateSerializer.class)  
	private LocalDate transactionDate;
	
	@Transient
	private LocalTime transactionTime;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idProjeto")
	@ApiModelProperty(name="Projeto", required = false, value = "Projeto envolvido na transação")
	private Projeto projeto;
	
	public Transacao(){
		
	}
	public Transacao(Long id, Long codTransacao, Usuario usuario, BigDecimal value, Long codCard,
			LocalDate transactionDate, LocalTime transactionTime, Projeto projeto) {
		super();
		this.id = id;
		this.codTransacao = codTransacao;
		this.usuario = usuario;
		this.value = value;
		this.codCard = codCard;
		this.transactionDate = transactionDate;
		this.transactionTime = transactionTime;
		this.projeto = projeto;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BigDecimal getValue() {
		return value;
	}
	public BigDecimal getValueIsyBuy() {
		return value != null? value.multiply(new BigDecimal(100)): value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public Long getCodCard() {
		return codCard;
	}
	public void setCodCard(Long codCard) {
		this.codCard = codCard;
	}
	public Long getCodCheckin() {
		return codCheckin;
	}
	public void setCodCheckin(Long codCheckin) {
		this.codCheckin = codCheckin;
	}
	public LocalDate getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(LocalDate transactionDate) {
		this.transactionDate = transactionDate;
	}
	public LocalTime getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(LocalTime transactionTime) {
		this.transactionTime = transactionTime;
	}
	public Projeto getProjeto() {
		return projeto == null? new Projeto():projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public Long getCodTransacao() {
		return codTransacao;
	}
	public void setCodTransacao(Long codTransacao) {
		this.codTransacao = codTransacao;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
