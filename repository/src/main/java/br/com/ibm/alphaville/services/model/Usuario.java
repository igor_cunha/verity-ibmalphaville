package br.com.ibm.alphaville.services.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(dataType="String", name="name", required = true, value = "Nome do usuário")
	@NotNull
	private String name;
	
	@ApiModelProperty(dataType="String", name="email", required = true, value = "E-mail")
	@NotNull
	private String email;
	
	@ApiModelProperty(dataType="String", name="password", required = true, value = "Senha")
	@NotNull
	private String password;
	
	@ApiModelProperty(dataType="String", name="cpf", required = true, value = "Número do CPF")
	@NotNull
	private String cpf;
	
	@ApiModelProperty(dataType="String", name="phone", required = true, value = "Número do telefone")
	@NotNull
	private String phone;
	
	private String hash;
	
	private Long codUser;
	
	public Usuario() {
	}

	public Usuario(String name, String email, String password, String cpf, String phone) {

		this.name = name;
		this.email = email;
		this.password = password;
		this.cpf = cpf;
		this.phone = phone;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Long getCodUser() {
		return codUser;
	}

	public void setCodUser(Long codUser) {
		this.codUser = codUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
