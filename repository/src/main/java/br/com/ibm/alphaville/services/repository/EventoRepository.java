package br.com.ibm.alphaville.services.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.ibm.alphaville.services.model.Evento;
import io.swagger.annotations.Api;

/**
 * Repositório criado em runtime pelo Spring Data - DataSource.
 * 
 */
@Api
//@Component
public interface EventoRepository /*extends CrudRepository<Evento, Long>*/ {

	List<Evento> findByNome(@Param("nome") String nome);

	@Query("select e from Evento e where e.data = :data")
	List<Evento> findByData(@Param("data") Date data);
}