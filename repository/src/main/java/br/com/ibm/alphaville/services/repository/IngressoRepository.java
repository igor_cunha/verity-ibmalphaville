package br.com.ibm.alphaville.services.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.ibm.alphaville.services.model.Ingresso;

public interface IngressoRepository extends CrudRepository<Ingresso, Long> {

}
