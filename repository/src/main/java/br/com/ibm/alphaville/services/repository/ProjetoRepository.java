package br.com.ibm.alphaville.services.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.ibm.alphaville.services.model.Projeto;

public interface ProjetoRepository extends CrudRepository<Projeto, Long> {
	Projeto findById(Long id);
	
	List<Projeto> findByAtivoIsTrueAndPublicadoIsTrueOrderByTipoProjeto();
}
