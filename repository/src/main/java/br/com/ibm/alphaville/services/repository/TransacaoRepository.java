package br.com.ibm.alphaville.services.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.ibm.alphaville.services.model.Transacao;

public interface TransacaoRepository extends CrudRepository<Transacao, Long> {
	
	@Query("select t, p from Transacao t inner join t.projeto p where t.codTransacao = :codTransacao")
	Transacao findByCodTransacao(@Param("codTransacao") Long codTransacao);
}
