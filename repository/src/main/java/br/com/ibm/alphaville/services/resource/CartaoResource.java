package br.com.ibm.alphaville.services.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.bo.CartaoBO;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Cartao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Rest Service para o domínio de Cartão.
 * 
 * @author igor.almeida
 */
@Api
@Component
@Path("/cartao")
public class CartaoResource {

	private final static Logger LOG = LoggerFactory.getLogger(CartaoResource.class);

	@Inject
	private CartaoBO cartaoBO;

	@GET
	@Path("/me/{hash}")
	@ApiOperation(value = "Busca os cartões do usuário pelo seu hash.", notes = "Essa operação busca os cartões do usuário, a partir do hash informado.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cartões encontrados."),
			@ApiResponse(code = 404, message = "Cartão não encontrado.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(
			@PathParam("hash") @ApiParam(name = "hash", required = true, value = "Hash do usuário") String hash) {
		try {
			List<Cartao> cartoes = cartaoBO.findCartaoByHash(hash);
			if (cartoes != null && !cartoes.isEmpty()) {
				return Response.ok().entity(cartoes).build();
			} else {
				return Response.status(404).entity(new Cartao()).build();
			}
		} catch (NotAuthorizedException e) {
			return Response.status(401).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
	}
	@POST
	@ApiOperation(value = "Salva um novo cartão.", notes = "Operação responsável por criar um novo cartão.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cartão criado com sucesso."),
			@ApiResponse(code = 400, message = "Ocorreram problemas ao criar o novo cartão.") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvarCartao(@ApiParam(name = "Cartão", required = true, value = "cartao") Cartao cartao) {
		try {
			cartaoBO.save(cartao);
		} catch (BusinessException | BadRequestException e) {
			return Response.status(400).entity(e.getMessage()).build();
		}catch (NotAuthorizedException e) {
			return Response.status(401).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.ok().entity(cartao).build();
	}
	
	@DELETE
	@Path("/{hash}/{codCard}")
	@ApiOperation(value = "Deleta os cartões do usuário pelo seu hash.", notes = "Essa operação deleta os cartões do usuário, a partir do hash informado.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Cartão deletado."),
			@ApiResponse(code = 404, message = "Cartão não deletado.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteById(
			@PathParam("hash") @ApiParam(name = "hash", required = true, value = "Hash do usuário") String hash, 
			@PathParam("codCard") @ApiParam(name = "codCard", required = true, value = "Código do Cartão") Long codCard) {
		try {
			cartaoBO.deleteCard(hash, codCard);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
		return Response.noContent().build();
	}
}
