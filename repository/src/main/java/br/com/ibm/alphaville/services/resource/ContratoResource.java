package br.com.ibm.alphaville.services.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.client.ContratoClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@Component
@Path("/contrato")
public class ContratoResource {
	private final static Logger LOG = LoggerFactory.getLogger(ContratoResource.class);
	
	@Inject
	private ContratoClient contratoClient;
	
	@GET
	@Path("/uso")
	@ApiOperation(value = "Busca a politica de uso", notes = "Essa operação busca a politica de uso.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Politica encontrada."),
			@ApiResponse(code = 404, message = "Politica não encontrada.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUso() {
		try {
			return Response.ok().entity(contratoClient.getUserTerms()).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
	}
	@GET
	@Path("/privacidade")
	@ApiOperation(value = "Busca a politica de privacidade.", notes = "Essa operação busca a politica de privacidade", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Politica encontrada."),
			@ApiResponse(code = 404, message = "Politica não encontrada.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPrivacidade() {
		try {
			return Response.ok().entity(contratoClient.getPrivacyPolicy()).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
	}
	@GET
	@Path("/troca")
	@ApiOperation(value = "Busca a politica de troca", notes = "Essa operação busca a politica de troca", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Politica encontrada."),
			@ApiResponse(code = 404, message = "Politica não encontrada.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTroca() {
		try {
			return Response.ok().entity(contratoClient.getExchangePolicy()).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
	}
}
