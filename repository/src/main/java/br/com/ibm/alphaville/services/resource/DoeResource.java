package br.com.ibm.alphaville.services.resource;

import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.swagger.annotations.Api;

/**
 * Rest Service para o domínio de Eventos.
 * 
 */
@Api
@Component
@Path("/doe")
public class DoeResource {
	private final static Logger LOG = LoggerFactory.getLogger(DoeResource.class);
	
//	@POST
//	@ApiOperation(value = "Realizar uma doação.", notes = "Operação responsável por realizar uma doação.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "Doação realizada com sucesso."),
//			@ApiResponse(code = 500, message = "Ocorreram problemas ao realizar uma doação.") })
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response donate(@ApiParam(name = "HashUser", required = true, value = "hashUser") String hashUser,
//			@ApiParam(name = "Value", required = true, value = "value") String value,
//			@ApiParam(name = "CartaoId", required = true, value = "cartaoId") Integer cartaoId) {
//		try {
//			evento = eventoBO.save(evento);
//		} catch (Exception e) {
//			return Response.status(500).entity(evento).build();
//		}
//		return Response.ok().entity(evento).build();
//	}
}
