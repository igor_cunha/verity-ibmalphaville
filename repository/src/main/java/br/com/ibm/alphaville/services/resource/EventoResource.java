package br.com.ibm.alphaville.services.resource;

import javax.ws.rs.Path;

import io.swagger.annotations.Api;

/**
 * Rest Service para o domínio de Eventos.
 * 
 */
@Api
//@Component
@Path("/eventos")
public class EventoResource {/*

	private final static Logger LOG = LoggerFactory.getLogger(EventoResource.class);

	//@Inject
	private EventoBO eventoBO;

	@GET
	@ApiOperation(value = "Operação que obtém todos os eventos da IBM Alphaville.", notes = "Busca, sem parâmetros, todos os eventos da IBM Alphaville", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta efetuada com sucesso.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		final Iterable<Evento> iterable = eventoBO.findAll();
		return Response.ok().entity(iterable).build();
	}

	@GET
	@Path("/{idEvento}")
	@ApiOperation(value = "Busca um evento da IBM Alphaville pelo seu ID.", notes = "Essa operação busca um evento, a partir do ID informado.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Evento encontrado."),
			@ApiResponse(code = 404, message = "Evento não encontrado.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(
			@PathParam("idEvento") @ApiParam(name = "idEvento", example = "001", required = true, value = "Id do evento") Long idEvento) {
		Evento evento = eventoBO.findOne(idEvento);

		if (evento != null) {
			return Response.ok().entity(evento).build();
		} else {
			return Response.status(404).entity(new Evento()).build();
		}
	}

	@GET
	@Path("/data/{dataEvento}")
	@ApiOperation(value = "Busca eventos na data informada.", notes = "Retorna todos os eventos, a partir da data informada como parâmetro.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Eventos encontrados."),
			@ApiResponse(code = 404, message = "Nenhum evento encontrado.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByData(
			@PathParam("dataEvento") 
			@ApiParam(name = "dataEvento", example = "30-12-2017(dd-MM-yyyy)", required = true, value = "Data do evento no formato dd-MM-yyyy") 
			String dataEvento) {
		List<Evento> eventos;
		try {
			eventos = eventoBO.findByData(dataEvento);
		} catch (ParseException e) {
			return Response.status(HttpStatus.BAD_REQUEST.value()).entity("Data em formato inválido. O formato correto é dd-MM-yyyy").build();
		}
		return Response.ok().entity(eventos).build();
	}

	@POST
	@ApiOperation(value = "Salva um novo evento.", notes = "Operação responsável por gerar um novo evento.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Evento criado com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao criar o novo evento.") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response salvarEvento(@ApiParam(name = "Evento", required = true, value = "evento") Evento evento) {
		try {
			evento = eventoBO.save(evento);
		} catch (Exception e) {
			return Response.status(500).entity(evento).build();
		}
		return Response.ok().entity(evento).build();
	}

	@PUT
	@ApiOperation(value = "Altera um evento existente.", notes = "Operação será responsável por alterar um evento existente no banco de dados.", consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Evento alterado com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao alterar o evento.") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateEvento(@ApiParam(name = "Evento", required = true, value = "evento") Evento evento) {
		try {
			eventoBO.update(evento);
			return Response.ok().build();
		} catch (BusinessException e) {
			return Response.status(HttpStatus.NOT_FOUND.value()).entity("Evento não localizado para atualização").build();
		}
	}

	@DELETE
	@Path("/{idEvento}")
	@ApiOperation(value = "Remove um evento existente.", notes = "Essa operação será responsável por remover um evento existente no banco de dados.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Evento removido com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao remover o evento.") })
	public Response deleteEvento(
			@PathParam("idEvento") @ApiParam(name = "idEvento", example = "001", required = true, value = "Código do evento") Long idEvento) {
		try {
			eventoBO.delete(idEvento);
			return Response.ok().build();
		} catch (BusinessException e) {
			return Response.status(HttpStatus.NOT_FOUND.value()).entity("Evento não localizado para deleção").build(); 
		}
		
	}
*/}
