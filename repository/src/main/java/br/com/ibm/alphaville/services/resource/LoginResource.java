package br.com.ibm.alphaville.services.resource;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.bo.LoginBO;
import br.com.ibm.alphaville.services.model.Usuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@Component
@Path("/login")
public class LoginResource {
	private final static Logger LOG = LoggerFactory.getLogger(UsuarioResource.class);
	
	@Inject
	private LoginBO loginBO;
	
	@POST
	@ApiOperation(value = "Logar o usuário.", notes = "Operação responsável por logar um usuário.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário logado com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao logar o usuário.") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cadastrarUsuario(@ApiParam(name = "login", required = true, value = "login") Usuario usuario) {
		Usuario usuarioCadastrado = null;
		try {
			usuarioCadastrado = loginBO.login(usuario);
		} catch (BadRequestException br) {
			return Response.status(400).entity(br.getMessage()).build();
		} catch(Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
		return Response.ok().entity(usuarioCadastrado).build();
	}
	
	@POST
	@ApiOperation(value = "Recuperar senha do usuário.", notes = "Operação responsável por Recuperar senha do usuário.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "E-mail para Recuperar senha do usuário enviado com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao Recuperar senha do usuário.") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/esqueciSenha")
	public Response enviarLinkRenovacaoSenha(@ApiParam(name = "recoverPassword", required = true, value = "recoverPassword") Usuario usuario) {
		try {
			loginBO.recoverPassword(usuario.getEmail());
		} catch(BadRequestException br) {
			return Response.status(400).entity(br.getMessage()).build(); 
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).entity(usuario.getEmail()).build();
		}
		return Response.ok().entity(usuario).build();
	}
	
}
