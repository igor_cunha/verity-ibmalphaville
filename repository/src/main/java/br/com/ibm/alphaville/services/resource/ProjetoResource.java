package br.com.ibm.alphaville.services.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.bo.ProjetoBO;
import br.com.ibm.alphaville.services.model.Projeto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@Component
@Path("/projeto")
public class ProjetoResource {
	private final static Logger LOG = LoggerFactory.getLogger(ProjetoResource.class);
	
	@Inject
	private ProjetoBO projetoBO;
	
	@GET
	@ApiOperation(value = "Consulta todos os eventos do banco", notes = "Operação responsável por consultar todos os eventos do banco", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Consulta realizada com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao realizar a consulta")})
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response transfer(@ApiParam(name = "Projeto", required = true, value = "projeto") Projeto projeto) {
		try {
			List<Projeto> projetos = projetoBO.findPublishedEvents();
			return Response.ok().entity(projetos).build();
			//return Response.ok().entity(transacaoBO.transfer(transacao)).build();
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).entity(projeto).build();
		}
	}
}
