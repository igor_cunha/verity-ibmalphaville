package br.com.ibm.alphaville.services.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.bo.TransacaoBO;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Transacao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Rest Service para o domínio de Transacao.
 * @author igor.almeida
 */
@Api
@Component
@Path("/transacao")
public class TrasacaoResource {

	private final static Logger LOG = LoggerFactory.getLogger(TrasacaoResource.class);

	@Inject
	private TransacaoBO transacaoBO;

	@POST
	@ApiOperation(value = "Realiza uma nova transação.", notes = "Operação responsável por realizar uma transação.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Transação realizada com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao realizar uma transação."),
			@ApiResponse(code = 400, message = "Dados incorretos foram passados, não é possível completar a transação.")})
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response transfer(@ApiParam(name = "Transacao", required = true, value = "transacao") Transacao transacao) {
		try {
			return Response.ok().entity(transacaoBO.transfer(transacao)).build();
		} catch(ForbiddenException f){
			return Response.status(403).entity(f.getMessage()).build();
		} catch(BusinessException | BadRequestException be){
			return Response.status(400).entity(be).build();
		}catch (NotFoundException e) {
			return Response.status(404).build();
		}catch (NotAuthorizedException e) {
			return Response.status(401).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).entity(e.getMessage()).build();
		}
	}
	@GET
	@Path("/{hash}")
	@ApiOperation(value = "Busca os recibos do usuário pelo seu hash.", notes = "Essa operação busca os recibos do usuário, a partir do hash informado.", produces = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "RecibosTransação encontrados."),
			@ApiResponse(code = 404, message = "ReciboTransação não encontrado.") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByhash(
			@PathParam("hash") @ApiParam(name = "hash", required = true, value = "Hash do usuário") String hash) {
		try {
			List<Transacao> receipts = transacaoBO.listReceiptByHash(hash);
			if (receipts != null && !receipts.isEmpty()) {
				return Response.ok().entity(receipts).build();
			} else {
				return Response.status(404).entity(new Transacao()).build();
			}
		} catch (NotAuthorizedException e) {
			return Response.status(401).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).build();
		}
	}
}
