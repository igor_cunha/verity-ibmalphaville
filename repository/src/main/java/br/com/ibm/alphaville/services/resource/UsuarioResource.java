package br.com.ibm.alphaville.services.resource;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ibm.alphaville.services.bo.UsuarioBO;
import br.com.ibm.alphaville.services.exceptions.BusinessException;
import br.com.ibm.alphaville.services.model.Usuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@Component
@Path("/usuarios")
public class UsuarioResource {
	private final static Logger LOG = LoggerFactory.getLogger(UsuarioResource.class);
	
	@Inject
	private UsuarioBO usuarioBO;
	
	@POST
	@ApiOperation(value = "Salva um novo usuário.", notes = "Operação responsável por gerar um novo usuário.", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON, response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário cadastrado com sucesso."),
			@ApiResponse(code = 500, message = "Ocorreram problemas ao cadastrar o novo usuário.") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cadastrarUsuario(@ApiParam(name = "usuario", required = true, value = "usuario") Usuario usuario) {
		try {
			usuarioBO.save(usuario);
		} catch(BusinessException | BadRequestException e){
			return Response.status(400).entity(e.getMessage()).build();
		}catch (Exception e) {
			LOG.error(e.getMessage());
			return Response.status(500).entity(usuario.getHash()).build();
		}
		return Response.ok().entity(usuario).build();
	}
}
