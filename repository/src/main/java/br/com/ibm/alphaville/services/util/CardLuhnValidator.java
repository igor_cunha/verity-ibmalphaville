package br.com.ibm.alphaville.services.util;

public class CardLuhnValidator {

	/**
	 * Validate a number string using Luhn algorithm
	 * 
	 * @param numberString
	 * @return
	 */
	public static boolean validate(String numberString) {
		return checkSum(numberString) == 0;
	}

	/**
	 * Generate check digit for a number string.
	 * 
	 * @param numberString
	 * @return
	 */
	public static int checkSum(String numberString) {
		numberString = numberString.replace(" ", "");
		int sum = 0;

		int[] ints = new int[numberString.length()];
		for (int i = 0; i < numberString.length(); i++) {
			ints[i] = Integer.parseInt(numberString.substring(i, i + 1));
		}
		for (int i = ints.length - 2; i >= 0; i = i - 2) {
			int j = ints[i];
			j = j * 2;
			if (j > 9) {
				j = j % 10 + 1;
			}
			ints[i] = j;
		}
		for (int i = 0; i < ints.length; i++) {
			sum += ints[i];
		}
		return sum % 10;
	}
}
