package br.com.ibm.alphaville.services.util;

import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class DataUtil {
	public static String localDateToDiaSemanaEDataEscrito(LocalDate value) {
		Instant instant = value.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR"));
		String dt = formatador.format(res);
		return dt;
	}
	public static String localDateToDiaSemanaEscrito(LocalDate value) {
		Instant instant = value.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR"));
		String dt = formatador.format(res).replaceFirst("[0-9].*", "");
		return dt;
	}
	public static String localDateToDataEscrito(LocalDate value) {
		Instant instant = value.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		Date res = Date.from(instant);
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR"));
		String dt = formatador.format(res).replaceAll(".*,", "");
		return dt;
	}
}
