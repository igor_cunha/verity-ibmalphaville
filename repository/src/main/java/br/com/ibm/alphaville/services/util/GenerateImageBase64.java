package br.com.ibm.alphaville.services.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

public class GenerateImageBase64 {
	//static String pathImagem = "C:\\Users\\yuri.silva\\Documents\\ibmalphavilleapp\\IBM\\src\\assets\\imgs\\file.jpeg";
	//static String pathImagem = "C:\\Users\\yuri.silva\\Documents\\ibmalphavilleapp\\IBM\\src\\assets\\imgs\\w8.jpeg";            
	
	static String pathImagem = "C:\\Users\\yuri.silva\\Documents\\ibmalphavilleapp\\IBM\\src\\assets\\imgs\\who.jpeg";
	//static String pathImagem = "C:\\Users\\yuri.silva\\Documents\\ibmalphavilleapp\\IBM\\src\\assets\\imgs\\w11.jpeg";
	
	//static String pathImagem = "C:\\Users\\yuri.silva\\Documents\\ibmalphavilleapp\\IBM\\src\\assets\\imgs\\w12.jpeg";
	//static String pathImagem = "C:\\Users\\yuri.silva\\Documents\\ibmalphavilleapp\\IBM\\src\\assets\\imgs\\w13.jpeg";
	public static void main(String[] args) throws IOException {
		BufferedImage imm = ImageIO.read(new File(pathImagem));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		//use another encoding if JPG is innappropriate for you
		ImageIO.write(imm, "jpg", baos );
		baos.flush();
		byte[] immAsBytes = baos.toByteArray();
		baos.close();
		String imageDataString = Base64.getEncoder().encodeToString(immAsBytes);
		System.out.println("data:image/jpeg;base64,"+imageDataString);
	}

}
