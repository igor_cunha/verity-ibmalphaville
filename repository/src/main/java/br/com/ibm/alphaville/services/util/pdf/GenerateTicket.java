package br.com.ibm.alphaville.services.util.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class GenerateTicket {
	private final static String SAGOE_UI_BOLD = File.separator+"fonts"+File.separator+"SegoeUIBold.ttf";
	private final static String SAGOE_UI_SEMI_BOLD = File.separator+"fonts"+File.separator+"segoeuisemibold2.ttf";

	public static void main(String[] args) {
		try {
		    
		    
		    
		    /*TODO: 
		     * Receber um projeto, 
		     * criar ingresso BD (associar a transacao, obter codigo ingresso), 
		     * popular Template
		     * Gerar Ingresso
		     * Retorna Ingresso(formato para anexo verificar)
*/		    
		    IngressoTemplate ingresso = new IngressoTemplate();
		    ingresso.setTitulo("Baile de favela");
		    ingresso.setValor("R$ 100,00");
		    ingresso.setLocal("Marconi");
		    ingresso.setEndereco("Av. Tamboré, 1511, Tamboré – 06460-000 – Barueri, SP");
		    ingresso.setDiaSemana("Sexta-Feira,");
		    ingresso.setData("30 de Setembro de 2018 | 19:00");
		    ingresso.setCodigo("000001");
		    
		  //Create PdfReader instance.
		    PdfReader templatePDF = 
				new PdfReader("C:\\Users\\igor.almeida\\Desktop\\projetos\\IBMAlphaville\\ingresso-template.pdf");	
 
		    //Create PdfStamper instance.
		    ByteArrayOutputStream ingressoByte = new ByteArrayOutputStream();
		    PdfStamper pdfStamper = new PdfStamper(templatePDF,ingressoByte);
		    
		    //Create BaseFont instance.
		    BaseFont fontSagoeBold = BaseFont.createFont(SAGOE_UI_BOLD, BaseFont.WINANSI,BaseFont.EMBEDDED);
		    BaseFont fontSagoeSemiBold = BaseFont.createFont(SAGOE_UI_SEMI_BOLD, BaseFont.WINANSI,BaseFont.EMBEDDED);
 
			//Contain the pdf data.
			PdfContentByte pageContentByte = pdfStamper.getOverContent(1);
 
			pageContentByte.beginText();
			
			//TITULO
			pageContentByte.setFontAndSize(fontSagoeBold, 22);
			pageContentByte.setTextMatrix(53, 774);
			pageContentByte.showText(ingresso.getTitulo().toUpperCase());
			
			//Valor
			pageContentByte.setFontAndSize(fontSagoeSemiBold, 18);
			pageContentByte.setTextMatrix(53, 750);
			pageContentByte.showText(ingresso.getValor().toUpperCase());
			
			//Local
			pageContentByte.setFontAndSize(fontSagoeBold, 18);
			pageContentByte.setTextMatrix(53, 700);
			pageContentByte.showText(ingresso.getLocal());
			
			//Endereco
			pageContentByte.setFontAndSize(fontSagoeSemiBold, 18);
			pageContentByte.setTextMatrix(53, 678);
			pageContentByte.showText(ingresso.getEndereco());
			
			//DiaSemana
			pageContentByte.setFontAndSize(fontSagoeSemiBold, 20);
			pageContentByte.setTextMatrix(53, 520);
			pageContentByte.showText(ingresso.getDiaSemana());
			
			//Data
			pageContentByte.setFontAndSize(fontSagoeSemiBold, 20);
			pageContentByte.setTextMatrix(53, 495);
			pageContentByte.showText(ingresso.getData());
			
			//Codigo
			pageContentByte.setFontAndSize(fontSagoeSemiBold, 20);
			pageContentByte.setTextMatrix(445, 510);
			pageContentByte.showText(ingresso.getCodigo());
			
			pageContentByte.endText();
 
		    //Close the pdfStamper.
		    pdfStamper.close();	
		    ingressoByte.writeTo(new FileOutputStream("C:\\Users\\igor.almeida\\ModifiedTest.pdf"));
		    System.out.println("PDF modified successfully.");
		} catch (Exception e) {
		    e.printStackTrace();
		}

	}

}
