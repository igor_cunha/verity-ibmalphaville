package br.com.ibm.alphaville.services.util.pdf;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import br.com.ibm.alphaville.services.util.DataUtil;

public class IngressoTemplate implements Serializable {

	private static final long serialVersionUID = 1L;
	private String titulo;
	private String valor;
	private String local;
	private String endereco;
	private String diaSemana;
	private String data;
	private String hora;
	private String codigo;
	
	public IngressoTemplate(String titulo, BigDecimal valor, String lugar, String endereco, LocalDate dataEvento,
			LocalTime hora, Long codigo) {
		setTitulo(titulo!= null? titulo.toUpperCase():"");
		setValor(valor!= null? "R$ "+valor.toString().replace(".", ","):"");
		setLocal(lugar!= null? "Local: "+lugar:"");
		setEndereco(endereco!=null? endereco:"");
		setDiaSemana(dataEvento!= null? DataUtil.localDateToDiaSemanaEscrito(dataEvento):"");
		setData(dataEvento!= null? DataUtil.localDateToDataEscrito(dataEvento).trim():"");
		setHora(hora!= null? hora.toString():"");
		setCodigo(codigo!= null? codigo.toString():"");
	}
	public IngressoTemplate() {
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getDiaSemana() {
		return diaSemana;
	}
	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}
	public String getData() {
		return data + " | " + getHora();
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getCodigo() {
		int qtdZero = 6 - this.codigo.length();
		String zero = "";
		while(qtdZero > 0){
			zero +="0";
			qtdZero--;
		}
		return zero+codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
