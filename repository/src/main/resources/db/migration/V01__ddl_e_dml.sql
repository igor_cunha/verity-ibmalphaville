/************************************************************************** 
* Verity TI
* --------------------------- 
* Criado por...:           Igor Almeida
* Em...........:           30/01/2018
* Projeto......:           IBMAlphaville
* Descrição....:           Script para criação das tabelas IBMAlphaville
* Tabelas envoldidas:      Todas
**************************************************************************/ 
DECLARE @nomeScript VARCHAR(MAX);
SET @nomeScript = '01. DDL IBMAlphaville'
PRINT 'Iniciando execução script ['+ @nomeScript +']'
BEGIN TRY
    BEGIN TRANSACTION;
		USE [IBMAlphaville]

		CREATE TABLE [Projeto](
			[id] [BIGINT] IDENTITY(1,1) NOT NULL,
			[idTipoProjeto] [BIGINT] NOT NULL,
			[titulo] [varchar](100) NULL,
			[descricao] [varchar](1000) NULL,
			[linkSite] [varchar](200) NULL,
			[img] [varchar](max) NULL,
			[valor] [decimal](15,2) NULL,
			[qtdIngresso] [INT] NULL,
			[qtdIngressoVendido] [INT] NULL,
			[lugar] [varchar](100) NULL,
			[endereco] [varchar](200) NULL,
			[dataEvento] [date] NULL,
			[horaEvento] [time] NULL,
			[dataPublicacao] [date] NULL,
			[dataCadastro] [date] DEFAULT GETDATE() NOT NULL,
			[publicado] [bit] DEFAULT 0 NOT NULL,	
			[ativo] [bit] DEFAULT 1 NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		CREATE TABLE [TipoProjeto](
			[id] [BIGINT] IDENTITY(1,1) NOT NULL,
			[descricao] [varchar](100) NOT NULL,
		 CONSTRAINT [PK_Projeto] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		CREATE TABLE [Transacao](
			[id] [BIGINT] IDENTITY(1,1) NOT NULL,
			[idProjeto] [BIGINT] NOT NULL,
			[codTransacao] [BIGINT] NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		CREATE TABLE [Ingresso](
			[id] [BIGINT] IDENTITY(1,1) NOT NULL,
			[idTransacao] [BIGINT] NOT NULL,
		PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		ALTER TABLE [Projeto]  WITH CHECK ADD  CONSTRAINT [FK_Projeto_TipoProjeto] FOREIGN KEY([idTipoProjeto])
		REFERENCES [TipoProjeto] ([id])
		
		ALTER TABLE [Projeto] CHECK CONSTRAINT [FK_Projeto_TipoProjeto]

		ALTER TABLE [Transacao]  WITH CHECK ADD  CONSTRAINT [FK_Transacao_Projeto] FOREIGN KEY([idProjeto])
		REFERENCES [Projeto] ([id])

		ALTER TABLE [Transacao] CHECK CONSTRAINT [FK_Transacao_Projeto]
		
		ALTER TABLE [Ingresso]  WITH CHECK ADD  CONSTRAINT [FK_Ingresso_Transacao] FOREIGN KEY([idTransacao])
		REFERENCES [Transacao] ([id])

		ALTER TABLE [Ingresso] CHECK CONSTRAINT [FK_Ingresso_Transacao]
		
		COMMIT TRANSACTION;
 
    PRINT 'Sucesso na execução do script ['+ @nomeScript +']'
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			 ROLLBACK TRANSACTION;
             
 		DECLARE @errorNumber INT;
		SET @errorNumber  = ERROR_NUMBER();
		DECLARE @errorLine INT;
		SET @errorLine = ERROR_LINE();
		DECLARE @errorMessage NVARCHAR(4000);
		SET @errorMessage = ERROR_MESSAGE();
		DECLARE @errorSeverity INT;
		SET @errorSeverity = ERROR_SEVERITY();
		DECLARE @errorState INT;
		SET @errorState = ERROR_STATE();
      
		PRINT 'ERRO na execução do script. [' + @nomeScript + ']'
		PRINT 'Número do erro: [' + CAST(@errorNumber AS VARCHAR(10)) + ']';
		PRINT 'Número da linha: [' + CAST(@errorLine AS VARCHAR(10)) + ']';
		PRINT 'Descrição do erro: [' + @errorMessage + ']';
 
		RAISERROR(@errorMessage, @errorSeverity, @errorState);
END CATCH
/************************************************************************** 
* Verity TI
* --------------------------- 
* Criado por...:           Igor Almeida
* Em...........:           30/01/2018
* Projeto......:           IBMAlphaville
* Descrição....:           Script para preenchimento das tabelas do banco IBMAlphaville
* Tabelas envoldidas:      TipoProjeto
**************************************************************************/ 

DECLARE @nomeScript2 VARCHAR(MAX);
SET @nomeScript2 = '02. DML IBMAlphaville'
PRINT 'Iniciando execução script ['+ @nomeScript2 +']'
BEGIN TRY
    BEGIN TRANSACTION;
		USE [IBMAlphaville]

		INSERT INTO TipoProjeto VALUES 
										('Doe'),
										('Missão'),
										('Evento')

		INSERT INTO Projeto (idTipoProjeto,titulo, publicado, dataPublicacao) VALUES
			(1, 'Doe', 1, GETDATE()),(2,'Missão',1, GETDATE())
											

		COMMIT TRANSACTION;
 
    PRINT 'Sucesso na execução do script ['+ @nomeScript2 +']'
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			 ROLLBACK TRANSACTION;
             
 		DECLARE @errorNumber2 INT;
		SET @errorNumber2  = ERROR_NUMBER();
		DECLARE @errorLine2 INT;
		SET @errorLine2 = ERROR_LINE();
		DECLARE @errorMessage2 NVARCHAR(4000);
		SET @errorMessage2 = ERROR_MESSAGE();
		DECLARE @errorSeverity2 INT;
		SET @errorSeverity2 = ERROR_SEVERITY();
		DECLARE @errorState2 INT;
		SET @errorState2 = ERROR_STATE();
      
		PRINT 'ERRO na execução do script. [' + @nomeScript2 + ']'
		PRINT 'Número do erro: [' + CAST(@errorNumber2 AS VARCHAR(10)) + ']';
		PRINT 'Número da linha: [' + CAST(@errorLine2 AS VARCHAR(10)) + ']';
		PRINT 'Descrição do erro: [' + @errorMessage2 + ']';
 
		RAISERROR(@errorMessage2, @errorSeverity2, @errorState2);
END CATCH