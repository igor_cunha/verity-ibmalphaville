/************************************************************************** 
* Verity TI
* --------------------------- 
* Criado por...:           Igor Almeida
* Em...........:           15/03/2018
* Projeto......:           IBMAlphaville
* Descrição....:           Script para alteração da tabela Transacao
* Tabelas envoldidas:      Transacao
**************************************************************************/ 
DECLARE @nomeScript VARCHAR(MAX);
SET @nomeScript = '03. ALTER IBMAlphaville'
PRINT 'Iniciando execução script ['+ @nomeScript +']'
BEGIN TRY
    BEGIN TRANSACTION;
		USE [IBMAlphaville]

		ALTER TABLE Transacao ADD valor Decimal(15,2) NULL;
		
		ALTER TABLE Transacao ADD dataTransacao Date NOT NULL DEFAULT getDate();
		
		COMMIT TRANSACTION;
 
    PRINT 'Sucesso na execução do script ['+ @nomeScript +']'
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			 ROLLBACK TRANSACTION;
             
 		DECLARE @errorNumber INT;
		SET @errorNumber  = ERROR_NUMBER();
		DECLARE @errorLine INT;
		SET @errorLine = ERROR_LINE();
		DECLARE @errorMessage NVARCHAR(4000);
		SET @errorMessage = ERROR_MESSAGE();
		DECLARE @errorSeverity INT;
		SET @errorSeverity = ERROR_SEVERITY();
		DECLARE @errorState INT;
		SET @errorState = ERROR_STATE();
      
		PRINT 'ERRO na execução do script. [' + @nomeScript + ']'
		PRINT 'Número do erro: [' + CAST(@errorNumber AS VARCHAR(10)) + ']';
		PRINT 'Número da linha: [' + CAST(@errorLine AS VARCHAR(10)) + ']';
		PRINT 'Descrição do erro: [' + @errorMessage + ']';
 
		RAISERROR(@errorMessage, @errorSeverity, @errorState);
END CATCH